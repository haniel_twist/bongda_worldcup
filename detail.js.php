<script type="text/javascript">
    $(document).ready(function() {

        $('#related-post .list-item-news-related-post').owlCarousel({
            loop: false,
            margin: 15,
            autoplay: false,
            autoplayTimeout: 8000,
            slideBy: 1,
            items: 3,
            responsive:{
                0:{
                    items:1,
                },
                768:{
                    items:3,
                },
                992:{
                    items:3,
                },
                1199:{
                    items:3,
                }
            }
        });
        // -- Control -- 
        var listRollnews_related = $("#related-post .list-item-news-related-post").owlCarousel();
        // Next
        $('#related-post .next').click(function(){
            listRollnews_related.trigger('next.owl.carousel');
            return false;
        });
        // Prev
        $('#related-post .prev').click(function(){
            listRollnews_related.trigger('prev.owl.carousel');
            return false;
        });


        $('#news-latest-post .list-item-latest-news-post').owlCarousel({
            loop: true,
            margin: 15,
            autoplay: false,
            autoplayTimeout: 8000,
            slideBy: 1,
            // animateOut: 'fadeOut',
            items: 1,
            responsive:{
                0:{
                    items:1,
                },
                768:{
                    items:3,
                },
                992:{
                    items:4,
                },
                1199:{
                    items:4,
                }
            }
        });
        // -- Control -- 
        var listRoll1 = $("#news-latest-post .list-item-latest-news-post").owlCarousel();
        // Next
        $('#news-latest-post .next').click(function(){
            listRoll1.trigger('next.owl.carousel');
            return false;
        });
        // Prev
        $('#news-latest-post .prev').click(function(){
            listRoll1.trigger('prev.owl.carousel');
            return false;
        });

    });
    
</script>