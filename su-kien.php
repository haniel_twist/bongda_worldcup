<!-- page su kien -->
<div class="page wrap-main-content wrap-page-event">
	<section class="container">
		<div class="advertise-top">
			<a href="#" title=""><img src="assets/img/home/advertise-tiki.jpg" alt="" title=""></a>
		</div>
		<nav class="breadcrumb list-breadcrumb">
		  	<a class="breadcrumb-item" href="#"><i class="fa fa-home"></i></a>
		  	<span class="breadcrumb-item active">Sự kiện</span>
		</nav>
		<!-- breadcrumb -->

		<div class="row row-0">
			<div class="col-lg-2">
				<div class="highlight-event">
					<div class="title">
						nổi bật
					</div>
					<div class="list-item">
						<div class="item">
							<a href="#">
								Củng cố tuyến giữa, Arsenal muốn rước cái tên từ đối thủ 'truyền kiếp'
							</a>
						</div>
						<div class="item">
							<a href="#">
								Củng cố tuyến giữa, Arsenal muốn rước cái tên từ đối thủ 'truyền kiếp'
							</a>
						</div>
						<div class="item">
							<a href="#">
								Củng cố tuyến giữa, Arsenal muốn rước cái tên từ đối thủ 'truyền kiếp'
							</a>
						</div>
						<div class="item">
							<a href="#">
								Củng cố tuyến giữa, Arsenal muốn rước cái tên từ đối thủ 'truyền kiếp'
							</a>
						</div>
						<div class="item">
							<a href="#">
								Củng cố tuyến giữa, Arsenal muốn rước cái tên từ đối thủ 'truyền kiếp'
							</a>
						</div>
					</div>
				</div>
				<div class="advertise-left">
					<div class="advertise">
						<a href="#">
							<img src="assets/img/alowwatch.jpg" alt="advertise" title="advertise">
						</a>
					</div>
					<div class="advertise">
						<a href="#">
							<img src="assets/img/adam.jpg" alt="advertise" title="advertise">
						</a>
					</div>
				</div>		
			</div> 
			<!-- end col-lg-2 -->

			<div class="col-lg-7">
				<div class="main-content main-content-page page-event page-ben-le box-page-photo">
					<div class="news-latest">
						<a href="#">
							<div class="content-left">
								<h3>Những thống kê “không thể ngờ” sau VL World Cup 2018 môn thể thao Vua không thể ngờ tới.</h3>
								<span class="border-title"></span>
								<p>Trải qua vòng loại World Cup 2018, FIFA đã thống kê ra những con số khiến những Fan của môn thể thao Vua không thể ngờ tới. môn thể thao Vua không thể ngờ tới.</p>
							</div>
							<div class="content-right">
								<div class="image">
									<div class="super-img loaded ratio-8x5" style="background-image: url('assets/img/news/img-latest.jpg')">
	                                    <img src="assets/img/news/img-latest.jpg" alt="">
	                                </div>
								</div>
							</div>
						</a>
					</div>
					<div class="list-photo">
						<div class="item">
							<a href="#">
								<div class="image">
									<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/news/img-latest.jpg')">
	                                    <img src="assets/img/news/img-latest.jpg" alt="">
	                                </div>
								</div>
								<div class="content">
									<h3>Muốn "chuồn" sang Trung Quốc, Claudecir gây áp lực với Quảng Nam FC 2018</h3>
								</div>
							</a>
						</div>
						<div class="item">
							<a href="#">
								<div class="image">
									<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/news/img-latest.jpg')">
	                                    <img src="assets/img/news/img-latest.jpg" alt="">
	                                </div>
								</div>
								<div class="content">
									<h3>Muốn "chuồn" sang Trung Quốc, Claudecir gây áp lực với Quảng Nam FC 2018</h3>
								</div>
							</a>
						</div>
						<div class="item">
							<a href="#">
								<div class="image">
									<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/news/img-latest.jpg')">
	                                    <img src="assets/img/news/img-latest.jpg" alt="">
	                                </div>
								</div>
								<div class="content">
									<h3>Muốn "chuồn" sang Trung Quốc, Claudecir gây áp lực với Quảng Nam FC 2018</h3>
								</div>
							</a>
						</div>
					</div>

					<div class="item-news wrap-news-item">
						<a href="#">
							<div class="content-left">
								<div class="image">
									<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/news/img-latest.jpg')">
	                                    <img src="assets/img/news/img-latest.jpg" alt="">
	                                </div>
								</div>
							</div>
							<div class="content-right">
								<h3>Nhìn lại những khoảnh khắc khiến tuyển Anh phải ‘độn thổ’ (Phần 2)</h3>
								<p>Bên cạnh những thành công bao giờ cũng có những điểm tối. Ta sẽ cùng nhìn lại những khoảnh khắc khiến Tam Sư phải muối mặt nhất.</p>
							</div>
						</a>
					</div>

					<div class="item-news wrap-news-item">
						<a href="#">
							<div class="content-left">
								<div class="image">
									<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/news/img-latest.jpg')">
	                                    <img src="assets/img/news/img-latest.jpg" alt="">
	                                </div>
								</div>
							</div>
							<div class="content-right">
								<h3>Nhìn lại những khoảnh khắc khiến tuyển Anh phải ‘độn thổ’ (Phần 2)</h3>
								<p>Bên cạnh những thành công bao giờ cũng có những điểm tối. Ta sẽ cùng nhìn lại những khoảnh khắc khiến Tam Sư phải muối mặt nhất.</p>
							</div>
						</a>
					</div>

					<div class="item-news wrap-news-item">
						<a href="#">
							<div class="content-left">
								<div class="image">
									<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/news/img-latest.jpg')">
	                                    <img src="assets/img/news/img-latest.jpg" alt="">
	                                </div>
								</div>
							</div>
							<div class="content-right">
								<h3>Nhìn lại những khoảnh khắc khiến tuyển Anh phải ‘độn thổ’ (Phần 2)</h3>
								<p>Bên cạnh những thành công bao giờ cũng có những điểm tối. Ta sẽ cùng nhìn lại những khoảnh khắc khiến Tam Sư phải muối mặt nhất.</p>
							</div>
						</a>
					</div>

					<div class="item-news wrap-news-item">
						<a href="#">
							<div class="content-left">
								<div class="image">
									<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/news/img-latest.jpg')">
	                                    <img src="assets/img/news/img-latest.jpg" alt="">
	                                </div>
								</div>
							</div>
							<div class="content-right">
								<h3>Nhìn lại những khoảnh khắc khiến tuyển Anh phải ‘độn thổ’ (Phần 2)</h3>
								<p>Bên cạnh những thành công bao giờ cũng có những điểm tối. Ta sẽ cùng nhìn lại những khoảnh khắc khiến Tam Sư phải muối mặt nhất.</p>
							</div>
						</a>
					</div>

					<div id="paging">
						<ul>
							<li><a href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li class="active"><a href="#">4</a></li>
							<li><a href="#">5</a></li>
							<li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>
						</ul>
					</div>

				</div>
			</div>
			<!-- end col-lg-7 -->

			<div class="col-lg-3">
				<?php require 'sidebar.php';?>
			</div>
		</div>
	</section>
</div>

