<!-- page lich thi dau -->
<div class="page wrap-main-content">
	<section class="container">
		<div class="advertise-top">
			<a href="#" title=""><img src="assets/img/home/advertise-tiki.jpg" alt="" title=""></a>
		</div>
		<nav class="breadcrumb list-breadcrumb">
		  	<a class="breadcrumb-item" href="#"><i class="fa fa-home"></i></a>
		  	<span class="breadcrumb-item active">Lịch thi đấu</span>
		</nav>
		<!-- breadcrumb -->

		<div class="row row-0">
			<div class="col-lg-2">
				<div class="advertise-left">
					<div class="advertise">
						<a href="#">
							<img src="assets/img/alowwatch.jpg" alt="advertise" title="advertise">
						</a>
					</div>
					<div class="advertise">
						<a href="#">
							<img src="assets/img/adam.jpg" alt="advertise" title="advertise">
						</a>
					</div>
				</div>		
			</div> 
			<!-- end col-lg-2 -->

			<div class="col-lg-7">
				<div class="main-content main-content-page page-schedule">
					<div class="list-day-schedule">
						<div class="" id="schedule">
							<div class="control">
				                <a class="prev" href="#prev"><i class="zmdi zmdi-caret-left"></i></a>
                				<a class="next" href="#next"><i class="zmdi zmdi-caret-right"></i></a>
				            </div><!-- .control -->
				            <div class="list-item-schedule owl-carousel">
				               	<div class="item">
				               		<a href="#">
										<div class="day">14</div>
										<div class="date">Thứ 2</div>
					               	</a>
				               	</div>
				               	<div class="item">
				               		<a href="#">
										<div class="day">15</div>
										<div class="date">Thứ 3</div>
					               	</a>
				               	</div>
				               	<div class="item">
				               		<a href="#">
										<div class="day">16</div>
										<div class="date">Thứ 4</div>
					               	</a>
				               	</div>
				               	<div class="item">
				               		<a href="#">
										<div class="day">17</div>
										<div class="date">Thứ 5</div>
					               	</a>
				               	</div>
				               	<div class="item">
				               		<a href="#">
										<div class="day">18</div>
										<div class="date">Thứ 6</div>
					               	</a>
				               	</div>
				               	<div class="item">
				               		<a href="#">
										<div class="day">19</div>
										<div class="date">Thứ 7</div>
					               	</a>
				               	</div>
				               	<div class="item">
				               		<a href="#">
										<div class="day">20</div>
										<div class="date">Chủ nhật</div>
					               	</a>
				               	</div>
				               	<div class="item">
				               		<a href="#">
										<div class="day">21</div>
										<div class="date">Thứ 2</div>
					               	</a>
				               	</div>
				               	<div class="item">
				               		<a href="#">
										<div class="day">22</div>
										<div class="date">Thứ 3</div>
					               	</a>
				               	</div>
				               	<div class="item">
				               		<a href="#">
										<div class="day">23</div>
										<div class="date">Thứ 4</div>
					               	</a>
				               	</div>
				               	<div class="item">
				               		<a href="#">
										<div class="day">24</div>
										<div class="date">Thứ 5</div>
					               	</a>
				               	</div>
				               	<div class="item">
				               		<a href="#">
										<div class="day">25</div>
										<div class="date">Thứ 6</div>
					               	</a>
				               	</div>
				               	<div class="item">
				               		<a href="#">
										<div class="day">26</div>
										<div class="date">Thứ 7</div>
					               	</a>
				               	</div>
				               	<div class="item">
				               		<a href="#">
										<div class="day">27</div>
										<div class="date">Chủ nhật</div>
					               	</a>
				               	</div>
				            </div>
						</div>
					</div>
					<!-- day slider -->

					<div class="banner-page">
						<img src="assets/img/banner-lich-thi-dau.jpg" alt="">
					</div>

					<div class="table-schedule">
						<div class="title-table">
							LƯỢT ĐẦU TIÊN Vòng bảng
						</div>
						<div class="details-table table-responsive">
							<table class="table"> 
						   		<thead> 
						   			<tr> 
						   				<th>Ngày</th> 
						   				<th>Giờ</th> 
						   				<th>Bảng</th> 
						   				<th>Trận đấu</th> 
						   				<!-- <th></th>  -->
						   			</tr> 
						   		</thead> 
						   		<tbody> 
						   			<tr>
						   				<th scope="row">
						   					14/6/2018
						   				</th>
						   				<td>22h00</td>
						   				<td>A</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/br.jpg')">
				                                            <img src="assets/img/sidebar/br.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   			<tr>
						   				<th rowspan="2" scope="row">
						   					14/6/2018
						   				</th>
						   				<td>22h00</td>
						   				<td>B</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/br.jpg')">
				                                            <img src="assets/img/sidebar/br.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   			<tr>
						   				<td>22h00</td>
						   				<td>C</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content"> Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   			<tr>
						   				<th rowspan="3" scope="row">
						   					14/6/2018
						   				</th>
						   				<td>22h00</td>
						   				<td>B</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Ả-Rập Xê-Út</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/br.jpg')">
				                                            <img src="assets/img/sidebar/br.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   			<tr>
						   				<td>22h00</td>
						   				<td>C</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   			<tr>
						   				<td>22h00</td>
						   				<td>D</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

									<tr>
						   				<th rowspan="3" scope="row">
						   					14/6/2018
						   				</th>
						   				<td>22h00</td>
						   				<td>B</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/br.jpg')">
				                                            <img src="assets/img/sidebar/br.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   			<tr>
						   				<td>22h00</td>
						   				<td>C</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   			<tr>
						   				<td>22h00</td>
						   				<td>D</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   			<tr>
						   				<th rowspan="4" scope="row">
						   					14/6/2018
						   				</th>
						   				<td>22h00</td>
						   				<td>B</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/br.jpg')">
				                                            <img src="assets/img/sidebar/br.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   			<tr>
						   				<td>22h00</td>
						   				<td>C</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   			<tr>
						   				<td>22h00</td>
						   				<td>D</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>
						   			<tr>
						   				<td>22h00</td>
						   				<td>D</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   		</tbody> 
						   	</table>
						</div>
					</div>

					<div class="table-schedule">
						<div class="title-table">
							LƯỢT thứ 2 Vòng bảng
						</div>
						<div class="details-table table-responsive">
							<table class="table"> 
						   		<thead> 
						   			<tr> 
						   				<th>Ngày</th> 
						   				<th>Giờ</th> 
						   				<th>Bảng</th> 
						   				<th>Trận đấu</th> 
						   				<th></th> 
						   			</tr> 
						   		</thead> 
						   		<tbody> 
						   			<tr>
						   				<th scope="row">
						   					14/6/2018
						   				</th>
						   				<td>22h00</td>
						   				<td>A</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/br.jpg')">
				                                            <img src="assets/img/sidebar/br.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   			<tr>
						   				<th rowspan="2" scope="row">
						   					14/6/2018
						   				</th>
						   				<td>22h00</td>
						   				<td>B</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/br.jpg')">
				                                            <img src="assets/img/sidebar/br.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   			<tr>
						   				<td>22h00</td>
						   				<td>C</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   			<tr>
						   				<th rowspan="3" scope="row">
						   					14/6/2018
						   				</th>
						   				<td>22h00</td>
						   				<td>B</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/br.jpg')">
				                                            <img src="assets/img/sidebar/br.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   			<tr>
						   				<td>22h00</td>
						   				<td>C</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   			<tr>
						   				<td>22h00</td>
						   				<td>D</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

									<tr>
						   				<th rowspan="3" scope="row">
						   					14/6/2018
						   				</th>
						   				<td>22h00</td>
						   				<td>B</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/br.jpg')">
				                                            <img src="assets/img/sidebar/br.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   			<tr>
						   				<td>22h00</td>
						   				<td>C</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   			<tr>
						   				<td>22h00</td>
						   				<td>D</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   			<tr>
						   				<th rowspan="4" scope="row">
						   					14/6/2018
						   				</th>
						   				<td>22h00</td>
						   				<td>B</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/br.jpg')">
				                                            <img src="assets/img/sidebar/br.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   			<tr>
						   				<td>22h00</td>
						   				<td>C</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   			<tr>
						   				<td>22h00</td>
						   				<td>D</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>
						   			<tr>
						   				<td>22h00</td>
						   				<td>D</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   		</tbody> 
						   	</table>
						</div>
					</div>

					<div class="table-schedule">
						<div class="title-table">
							LƯỢT thứ 3 Vòng bảng
						</div>
						<div class="details-table table-responsive">
							<table class="table"> 
						   		<thead> 
						   			<tr> 
						   				<th>Ngày</th> 
						   				<th>Giờ</th> 
						   				<th>Bảng</th> 
						   				<th>Trận đấu</th> 
						   				<th></th> 
						   			</tr> 
						   		</thead> 
						   		<tbody> 
						   			<tr>
						   				<th scope="row">
						   					14/6/2018
						   				</th>
						   				<td>22h00</td>
						   				<td>A</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/br.jpg')">
				                                            <img src="assets/img/sidebar/br.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   			<tr>
						   				<th rowspan="2" scope="row">
						   					14/6/2018
						   				</th>
						   				<td>22h00</td>
						   				<td>B</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/br.jpg')">
				                                            <img src="assets/img/sidebar/br.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   			<tr>
						   				<td>22h00</td>
						   				<td>C</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   			<tr>
						   				<th rowspan="3" scope="row">
						   					14/6/2018
						   				</th>
						   				<td>22h00</td>
						   				<td>B</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/br.jpg')">
				                                            <img src="assets/img/sidebar/br.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   			<tr>
						   				<td>22h00</td>
						   				<td>C</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   			<tr>
						   				<td>22h00</td>
						   				<td>D</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

									<tr>
						   				<th rowspan="3" scope="row">
						   					14/6/2018
						   				</th>
						   				<td>22h00</td>
						   				<td>B</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/br.jpg')">
				                                            <img src="assets/img/sidebar/br.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   			<tr>
						   				<td>22h00</td>
						   				<td>C</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   			<tr>
						   				<td>22h00</td>
						   				<td>D</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   			<tr>
						   				<th rowspan="4" scope="row">
						   					14/6/2018
						   				</th>
						   				<td>22h00</td>
						   				<td>B</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/br.jpg')">
				                                            <img src="assets/img/sidebar/br.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   			<tr>
						   				<td>22h00</td>
						   				<td>C</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   			<tr>
						   				<td>22h00</td>
						   				<td>D</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>
						   			<tr>
						   				<td>22h00</td>
						   				<td>D</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   		</tbody> 
						   	</table>
						</div>
					</div>
					
					<div class="table-schedule round-table">
						<div class="title-table">
							vòng tứ kết
						</div>
						<div class="details-table table-responsive">
							<table class="table"> 
						   		<thead> 
						   			<tr> 
						   				<th>Ngày</th> 
						   				<th>Giờ</th> 
						   				<th>Trận</th> 
						   				<th>Cặp đấu</th> 
						   			</tr> 
						   		</thead> 
						   		<tbody> 
						   			<tr>
						   				<th scope="row">
						   					14/6/2018
						   				</th>
						   				<td>22h00</td>
						   				<td>1</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/gray.jpg')">
				                                            <img src="assets/img/gray.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Thắng trận 1/8 - 1</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/gray.jpg')">
				                                            <img src="assets/img/gray.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Thắng trận 1/8 - 2</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   			<tr>
						   				<th rowspan="2" scope="row">
						   					14/6/2018
						   				</th>
						   				<td>22h00</td>
						   				<td>2</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/gray.jpg')">
				                                            <img src="assets/img/gray.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Thắng trận 1/8 - 1</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/gray.jpg')">
				                                            <img src="assets/img/gray.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Thắng trận 1/8 - 2</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   			<tr>
						   				<td>22h00</td>
						   				<td>2</td>
						   				<td>
											<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/gray.jpg')">
				                                            <img src="assets/img/gray.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Thắng trận 1/8 - 1</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/gray.jpg')">
				                                            <img src="assets/img/gray.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Thắng trận 1/8 - 2</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   			<tr>
						   				<th scope="row">
						   					14/6/2018
						   				</th>
						   				<td>22h00</td>
						   				<td>1</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/gray.jpg')">
				                                            <img src="assets/img/gray.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Thắng trận 1/8 - 1</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/gray.jpg')">
				                                            <img src="assets/img/gray.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Thắng trận 1/8 - 2</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   		</tbody> 
						   	</table>
						</div>
					</div>

					<div class="table-schedule round-table">
						<div class="title-table">
							vòng tứ kết
						</div>
						<div class="details-table table-responsive">
							<table class="table"> 
						   		<thead> 
						   			<tr> 
						   				<th>Ngày</th> 
						   				<th>Giờ</th> 
						   				<th>Trận</th> 
						   				<th>Cặp đấu</th> 
						   			</tr> 
						   		</thead> 
						   		<tbody> 
						   			<tr>
						   				<th scope="row">
						   					14/6/2018
						   				</th>
						   				<td>22h00</td>
						   				<td>1</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/gray.jpg')">
				                                            <img src="assets/img/gray.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Thắng trận 1/8 - 1</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/gray.jpg')">
				                                            <img src="assets/img/gray.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Thắng trận 1/8 - 2</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   			<tr>
						   				<th rowspan="2" scope="row">
						   					14/6/2018
						   				</th>
						   				<td>22h00</td>
						   				<td>2</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/gray.jpg')">
				                                            <img src="assets/img/gray.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Thắng trận 1/8 - 1</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/gray.jpg')">
				                                            <img src="assets/img/gray.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Thắng trận 1/8 - 2</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   			<tr>
						   				<td>22h00</td>
						   				<td>2</td>
						   				<td>
											<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/gray.jpg')">
				                                            <img src="assets/img/gray.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Thắng trận 1/8 - 1</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/gray.jpg')">
				                                            <img src="assets/img/gray.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Thắng trận 1/8 - 2</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   			<tr>
						   				<th scope="row">
						   					14/6/2018
						   				</th>
						   				<td>22h00</td>
						   				<td>1</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/gray.jpg')">
				                                            <img src="assets/img/gray.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Thắng trận 1/8 - 1</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/gray.jpg')">
				                                            <img src="assets/img/gray.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Thắng trận 1/8 - 2</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   		</tbody> 
						   	</table>
						</div>
					</div>

					<div class="table-schedule round-table">
						<div class="title-table">
							vòng bán kết
						</div>
						<div class="details-table table-responsive">
							<table class="table"> 
						   		<thead> 
						   			<tr> 
						   				<th>Ngày</th> 
						   				<th>Giờ</th> 
						   				<th>Trận</th> 
						   				<th>Cặp đấu</th> 
						   			</tr> 
						   		</thead> 
						   		<tbody> 
						   			<tr>
						   				<th scope="row">
						   					14/6/2018
						   				</th>
						   				<td>22h00</td>
						   				<td>1</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/gray.jpg')">
				                                            <img src="assets/img/gray.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Thắng trận tứ kết 1</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/gray.jpg')">
				                                            <img src="assets/img/gray.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Thắng trận tứ kết 2</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   			<tr>
						   				<th scope="row">
						   					14/6/2018
						   				</th>
						   				<td>22h00</td>
						   				<td>1</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/gray.jpg')">
				                                            <img src="assets/img/gray.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Thắng trận tứ kết 3</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/gray.jpg')">
				                                            <img src="assets/img/gray.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Thắng trận tứ kết 4</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>

						   		</tbody> 
						   	</table>
						</div>
					</div>

					<div class="table-schedule round-table">
						<div class="title-table">
							tranh 3-4
						</div>
						<div class="details-table table-responsive">
							<table class="table"> 
						   		<thead> 
						   			<tr> 
						   				<th>Ngày</th> 
						   				<th>Giờ</th> 
						   				<th>Trận</th> 
						   				<th>Cặp đấu</th> 
						   			</tr> 
						   		</thead> 
						   		<tbody> 
						   			<tr>
						   				<th scope="row">
						   					14/6/2018
						   				</th>
						   				<td>22h00</td>
						   				<td>1</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/gray.jpg')">
				                                            <img src="assets/img/gray.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Thua bán kết 1</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/gray.jpg')">
				                                            <img src="assets/img/gray.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Thua bán kết 2</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>
						   		</tbody> 
						   	</table>
						</div>
					</div>

					<div class="table-schedule round-table">
						<div class="title-table">
							Chung kết
						</div>
						<div class="details-table table-responsive">
							<table class="table"> 
						   		<thead> 
						   			<tr> 
						   				<th>Ngày</th> 
						   				<th>Giờ</th> 
						   				<th>Trận</th> 
						   				<th>Cặp đấu</th> 
						   			</tr> 
						   		</thead> 
						   		<tbody> 
						   			<tr>
						   				<th scope="row">
						   					14/6/2018
						   				</th>
						   				<td>22h00</td>
						   				<td>1</td>
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/gray.jpg')">
				                                            <img src="assets/img/gray.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Thắng bán kết 1</div>
						   						</div>
												
												<div class="team center">
													vs	
												</div>

						   						<div class="team team-right">
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/gray.jpg')">
				                                            <img src="assets/img/gray.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Thắng bán kết 2</div>
						   						</div>
						   					</div>
						   				</td>
						   			</tr>
						   		</tbody> 
						   	</table>
						</div>
					</div>
				</div>
			</div>
			<!-- end col-lg-7 -->

			<div class="col-lg-3">
				<?php require 'sidebar.php';?>
			</div>
		</div>
	</section>
</div>

