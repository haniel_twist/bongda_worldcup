$(document).ready(function() {
    // tab table-score
    $('#list-table-score').tabify();
    // tab page du doan
    $('#tab-page').tabify();
    // tab info
    $('#list-tab-info').tabify();

    // $('.modal-mqc').modal({
    //     keyboard: false,
    //     backdrop: 'static',
    //     show: false
    // });

    // onscroll
    window.onscroll = function() {
        var heightHead = $('#header').outerHeight();
        console.log(heightHead);
        var scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
        if (scrollTop > 197) {
            $('#header').addClass('header-sticky');
            $('#advLeft.absoluteAdvHome').css('top', heightHead);
            $('#advRight.absoluteAdvHome').css('top', heightHead);
            $('.wrap-main-content').css('padding-top', '210px');
        } else {
            $('#header').removeClass('header-sticky');
            $('#advLeft.absoluteAdvHome').css('top', '337px');
            $('#advRight.absoluteAdvHome').css('top', '337px');
            $('.wrap-main-content').css('padding-top', 0);
        }
    };

    $('.search-form .icon-search > a').click(function() {
        if ($(this).parent().hasClass('active')) {
            $(this).parent().removeClass('active');
        } else {
            $(this).parent().addClass('active');
            setTimeout(function() {
                $('#max-search input').focus();
            }, 500);
        }
        if ($('#max-search').hasClass('opened')) {
            $('#max-search').removeClass('opened');
        } else {
            $('#max-search').addClass('opened');
        }
        return false;
    });

    $('html').click(function(e) {
        var icon_search = $(".search-form");
        if (!icon_search.is(e.target) && icon_search.has(e.target).length === 0) {
            $('#max-search').removeClass('opened').prev().removeClass('active');
        }
    });

   // ---------------- Min menu page ----------------
	$('#min-menu-page li.have-child > a').click(function(){
		$(this).parent().find('ul:first').slideToggle(function(){
            if(!$(this).is(":hidden")){ // Open
                $(this).parent('.have-child').addClass('expand');
            }else{
                $(this).parent('.have-child').removeClass('expand');
            }
        });
		return false;
	});

	// ---------------- Filter ----------------
	$('#filter header .expand').click(function(){
		$('#filter main').slideToggle(300);
    });


    $('#result .list-item-result').owlCarousel({
        loop: true,
        margin: 36,
        autoplay: false,
        autoplayTimeout: 8000,
        slideBy: 1,
        items: 1
    });
    // -- Control -- 
    var listRoll = $("#result .list-item-result").owlCarousel();
    // Next
    $('#result .next').click(function(){
        listRoll.trigger('next.owl.carousel');
        return false;
    });
    // Prev
    $('#result .prev').click(function(){
        listRoll.trigger('prev.owl.carousel');
        return false;
    });


    $('#news-most-read .list-item-news-most-read').owlCarousel({
        loop: true,
        margin: 36,
        autoplay: false,
        autoplayTimeout: 8000,
        slideBy: 1,
        items: 1
    });
    // -- Control -- 
    var listRoll1 = $("#news-most-read .list-item-news-most-read").owlCarousel();
    // Next
    $('#news-most-read .next').click(function(){
        listRoll1.trigger('next.owl.carousel');
        return false;
    });
    // Prev
    $('#news-most-read .prev').click(function(){
        listRoll1.trigger('prev.owl.carousel');
        return false;
    });

    if ($('.main-content-page')) {
        $('.main-content-page .detail-content > p').each(function() {
            var clone_p = $(this).clone();
            clone_p.find('strong').remove();
            if (clone_p.html() == '') {
                $(this).find('strong').css({
                    'color': '#000',
                    'font-size': '20px',
                    'text-transform': 'uppercase'
                });
            }
        });
    }
    $('#back-to-top').on('click', function (e) {
        e.preventDefault();
        $('html,body').animate({
            scrollTop: 0
        }, 700);
    });

    // next prev tab automobiliste
    var count_total = document.getElementById('fq_count').innerHTML;
    console.log(count_total);
    var count = 1;
        $('#btn-prev').hide();
        $('#btn-next').on('click',function(){

            if(count < count_total)
            {

                $('.tab-pane').hide()
                count++;
                $('#tab'+count).show();
                $('#nav-tab > .active').next('li').find('a').trigger('click');
                $('#nav-tab > .active').prev('li').addClass('done');
                if(count > 1){
                    $('#btn-prev').show();
                }
                else {
                    $('#btn-prev').hide();
                }

            }
            if(count == count_total) {
                $('#btn-next').css('display', 'none');
                $('.valider-mon').css('display', 'block');
            }
        });

        $('#btn-prev').on('click',function(){
            if(count > 1)
            {
                $('.tab-pane').hide()
                count--;
                $('#tab'+count).show();
                $('#nav-tab > .active').prev('li').find('a').trigger('click');
                $('#nav-tab > .active').addClass('prev');
                $('#nav-tab > .active').next('li').removeClass('done');
                if(count < 4) {
                    $('#btn-next').show();
                    $('#btn-next').css('display', 'block');
                    $('.valider-mon').css('display', 'none');
                }
                else
                $('#btn-next').hide();
            }
            if (count == 1) {
                $('#btn-prev').hide();
            }
            if(count == count_total) {
                $('#btn-next').css('display', 'none');
                $('.valider-mon').css('display', 'block');
            }
        });


    
});