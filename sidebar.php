<!-- nav sidebar -->

<div id="nav-sidebar">
	<div class="world-cup">
		<div class="title">
			WORLD CUP RUSSIA 2018
		</div>
		<div id="result">
			<div class="control">
                <a class="prev" href="#prev"></a>
                <a class="next" href="#next"></a>
            </div><!-- .control -->
            <div class="list-item-result owl-carousel">
               	<div class="item-result">
               		<div class="time">
               			<span>0:13h00</span>,
               			<span>15/03/2018</span>
               		</div>
               		<div class="team-vs-team">
               			<div class="team team-left">
               				<div class="logo-team">
                                <img src="assets/img/sidebar/logo-team1.png" alt="logo">
               				</div>
               				<div class="score">4</div>
               				<p>Arsenal</p>
               			</div>
               			<div class="vs">vs</div>
               			<div class="team team-right">
               				<div class="logo-team">
                                <img src="assets/img/sidebar/logo-team-2.png" alt="logo">
               				</div>
               				<div class="score">4</div>
               				<p>Arsenal</p>
               			</div>
               		</div>
               	</div>

               	<div class="item-result">
               		<div class="time">
               			<span>0:13h00</span>,
               			<span>15/03/2018</span>
               		</div>
               		<div class="team-vs-team">
               			<div class="team team-left">
               				<div class="logo-team">
                                    <img src="assets/img/sidebar/logo-team1.png" alt="logo">
               				</div>
               				<div class="score">4</div>
               				<p>Arsenal</p>
               			</div>
               			<div class="vs">vs</div>
               			<div class="team team-right">
               				<div class="logo-team">
                                    <img src="assets/img/sidebar/logo-team-2.png" alt="logo">
               				</div>
               				<div class="score">4</div>
               				<p>Arsenal</p>
               			</div>
               		</div>
               	</div>
            </div>
		</div>

		<div class="table-score">
			<div class="title">
				Bảng xếp hạng
			</div>
			<ul id="list-table-score"> 
			   <li class="active"><a href="#a">a</a></li>
			   <li><a href="#b">b</a></li>
			   <li><a href="#c">c</a></li>
			   <li><a href="#d">d</a></li>
			   <li><a href="#e">e</a></li>
			   <li><a href="#f">f</a></li>
			   <li><a href="#g">g</a></li>
			   <li><a href="#h">h</a></li>

			</ul> 

			<!-- content a-->
			<div class="content" id="a">
			   	<table class="table table-bordered"> 
			   		<thead> 
			   			<tr> 
			   				<th>Đội</th> 
			   				<th>BT</th> 
			   				<th>BB</th> 
			   				<th>Hiệu số</th> 
			   				<th>Điểm</th> 
			   			</tr> 
			   		</thead> 
			   		<tbody> 
			   			<tr> 
			   				<th scope="row">
			   					<div class="name-team">
			   						<div class="image">
			   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
                                            <img src="assets/img/sidebar/spain.png" alt="logo">
                                        </div>
                                    </div>
                                    <div class="content">Span</div>
			   					</div>
			   				</th> 
			   				<td>0</td> 
			   				<td>0</td> 
			   				<td>0</td> 
			   				<td>0</td> 
			   			</tr> 

			   			<tr> 
			   				<th scope="row">
			   					<div class="name-team">
			   						<div class="image">
			   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/portugal.jpg')">
                                            <img src="assets/img/sidebar/portugal.jpg" alt="logo">
                                        </div>
                                    </div>
                                    <div class="content">Portugal</div>
			   					</div>
			   				</th> 
			   				<td>0</td> 
			   				<td>0</td> 
			   				<td>0</td> 
			   				<td>0</td> 
			   			</tr>
			   			<tr> 
			   				<th scope="row">
			   					<div class="name-team">
			   						<div class="image">
			   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/br.jpg')">
                                            <img src="assets/img/sidebar/br.jpg" alt="logo">
                                        </div>
                                    </div>
                                    <div class="content">Brazil</div>
			   					</div>
			   				</th> 
			   				<td>0</td> 
			   				<td>0</td> 
			   				<td>0</td> 
			   				<td>0</td> 
			   			</tr> 
			   			<tr> 
			   				<th scope="row">
			   					<div class="name-team">
			   						<div class="image">
			   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/br.jpg')">
                                            <img src="assets/img/sidebar/br.jpg" alt="logo">
                                        </div>
                                    </div>
                                    <div class="content">Brazil</div>
			   					</div>
			   				</th> 
			   				<td>0</td> 
			   				<td>0</td> 
			   				<td>0</td> 
			   				<td>0</td> 
			   			</tr> 
			   		</tbody> 
			   	</table>
			</div>

			<!-- content b-->
			<div class="content" id="b">
			   	<table class="table table-bordered"> 
			   		<thead> 
			   			<tr> 
			   				<th>Đội</th> 
			   				<th>BT</th> 
			   				<th>BB</th> 
			   				<th>Hiệu số</th> 
			   				<th>Điểm</th> 
			   			</tr> 
			   		</thead> 
			   		<tbody> 
			   			<tr> 
			   				<th scope="row">
			   					<div class="name-team">
			   						<div class="image">
			   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/costa.jpg')">
                                            <img src="assets/img/sidebar/costa.jpg" alt="logo">
                                        </div>
                                    </div>
                                    <div class="content">Span</div>
			   					</div>
			   				</th> 
			   				<td>0</td> 
			   				<td>0</td> 
			   				<td>0</td> 
			   				<td>0</td> 
			   			</tr> 

			   			<tr> 
			   				<th scope="row">
			   					<div class="name-team">
			   						<div class="image">
			   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
                                            <img src="assets/img/sidebar/spain.png" alt="logo">
                                        </div>
                                    </div>
                                    <div class="content">Span</div>
			   					</div>
			   				</th> 
			   				<td>0</td> 
			   				<td>0</td> 
			   				<td>0</td> 
			   				<td>0</td> 
			   			</tr> 
			   		</tbody> 
			   	</table>
			</div>
			<div class="note">
				BT - Bàn thắng BB - Bàn bại
			</div>
			<div class="view-all-score">
				<a href="#">Xem tất cả <i class="zmdi zmdi-trending-flat"></i></a>
			</div>
		</div>
	</div>
	<!-- end world cup -->
	
	<div class="news-worldcup">
		<div class="title">
			BẢN TIN WORLD CUP
		</div>
		<div class="main-news-worldcup">
			<a href="#">
				<div class="image">
					<img src="assets/img/sidebar/img-blog-news-video.jpg" alt="">
				</div>
				<div class="content">
					<div class="icon-video">
						<i class="fa fa-play" aria-hidden="true"></i>
					</div>
					<div class="title">
						Top 10 sự kiện bóng đá nổi bật năm 2017
					</div>
				</div>
			</a>
		</div>
	</div>

	<!-- Lich thi dau page child -->
	<div class="schedul sidebar-schedul">
		<h2>Lịch thi đấu</h2>
		<div class="list-item-schedul">
			<div class="item">
				<div class="title">
					<h3>Group A</h3>
					<p><span>14 Jun 2018</span> - <span>18:00</span></p>
				</div>
				<div class="group-item">
					<div class="group-1">
						<div class="ensign">
							<div class="super-img loaded ratio-3x2" style="background-image: url('assets/img/home/br.jpg')">
                                <img src="assets/img/home/br.jpg" alt="logo">
                            </div>
						</div>
						<p>BRA</p>
					</div>

					<div class="group-2">
						<div class="ensign">
							<div class="super-img loaded ratio-3x2" style="background-image: url('assets/img/home/costa.jpg')">
                                <img src="assets/img/home/costa.jpg" alt="logo">
                            </div>
						</div>
						<p>Ả-RẬP XÊ-ÚT</p>
					</div>
				</div>
			</div>

			<div class="item">
				<div class="title">
					<h3>Group A</h3>
					<p><span>14 Jun 2018</span> - <span>18:00</span></p>
				</div>
				<div class="group-item">
					<div class="group-1">
						<div class="ensign">
							<div class="super-img loaded ratio-3x2" style="background-image: url('assets/img/home/br.jpg')">
                                <img src="assets/img/home/br.jpg" alt="logo">
                            </div>
						</div>
						<p>BRA</p>
					</div>

					<div class="group-2">
						<div class="ensign">
							<div class="super-img loaded ratio-3x2" style="background-image: url('assets/img/home/costa.jpg')">
                                <img src="assets/img/home/costa.jpg" alt="logo">
                            </div>
						</div>
						<p>Costa</p>
					</div>
				</div>
			</div>

			<div class="item">
				<div class="title">
					<h3>Group A</h3>
					<p><span>14 Jun 2018</span> - <span>18:00</span></p>
				</div>
				<div class="group-item">
					<div class="group-1">
						<div class="ensign">
							<div class="super-img loaded ratio-3x2" style="background-image: url('assets/img/home/br.jpg')">
                                <img src="assets/img/home/br.jpg" alt="logo">
                            </div>
						</div>
						<p>Ả-RẬP XÊ-ÚT</p>
					</div>

					<div class="group-2">
						<div class="ensign">
							<div class="super-img loaded ratio-3x2" style="background-image: url('assets/img/home/costa.jpg')">
                                <img src="assets/img/home/costa.jpg" alt="logo">
                            </div>
						</div>
						<p>Costa</p>
					</div>
				</div>
			</div>

			<div class="item">
				<div class="title">
					<h3>Group A</h3>
					<p><span>14 Jun 2018</span> - <span>18:00</span></p>
				</div>
				<div class="group-item">
					<div class="group-1">
						<div class="ensign">
							<div class="super-img loaded ratio-3x2" style="background-image: url('assets/img/home/br.jpg')">
                                <img src="assets/img/home/br.jpg" alt="logo">
                            </div>
						</div>
						<p>BRA</p>
					</div>

					<div class="group-2">
						<div class="ensign">
							<div class="super-img loaded ratio-3x2" style="background-image: url('assets/img/home/costa.jpg')">
                                <img src="assets/img/home/costa.jpg" alt="logo">
                            </div>
						</div>
						<p>Costa</p>
					</div>
				</div>
			</div>	
		</div>
		<div class="view-all">
			<a href="#" title="">Xem tất cả</a>
		</div>
	</div>
	<!-- End lich thi dau page child -->

	<div class="advertise native">
		<a href="#">
			<img src="assets/img/sidebar/advertise-native.jpg" alt="advertise" title="advertise">
		</a>
	</div>

	<div class="advertise native">
		<a href="#">
			<img src="assets/img/sidebar/img-swanpark.jpg" alt="advertise" title="advertise">
		</a>
	</div>
	
	<div class="list-funny tab-content">
		<div id="fq_count">3</div>
		<div id="tab1" class="tab-pane active">
			<div class="funny-question">
				<div class="title">Đố vui</div>
				<div class="question">
					Đội tuyển nào sau đây chỉ mới có được một lần duy nhất vinh dự tranh tài tại vòng chung kết World Cup?
				</div>
				<div class="answer">
					<ul>
						<li><a href="#">A. <span>Brazil</span></a></li>
						<li><a href="#">B. <span>Togo</span></a></li>
						<li><a href="#">C. <span>Croatia</span></a></li>
						<li><a href="#">D. <span>Zaire</span></a></li>
					</ul>
				</div>
			</div>
		</div>
		<div id="tab2" class="tab-pane">
			<div class="funny-question">
				<div class="title">Đố vui</div>
				<div class="question">
					Đội tuyển nào sau đây?
				</div>
				<div class="answer">
					<ul>
						<li><a href="#">A. <span>Brazil</span></a></li>
						<li><a href="#">B. <span>Togo</span></a></li>
						<li><a href="#">C. <span>Croatia</span></a></li>
						<li><a href="#">D. <span>Zaire</span></a></li>
					</ul>
				</div>
			</div>
		</div>
		<div id="tab3" class="tab-pane">
			<div class="funny-question">
				<div class="title">Đố vui</div>
				<div class="question">
					Đội tuyển nào sau đây chỉ mới có được một lần duy ?
				</div>
				<div class="answer">
					<ul>
						<li><a href="#">A. <span>Brazil</span></a></li>
						<li><a href="#">B. <span>Togo</span></a></li>
						<li><a href="#">C. <span>Croatia</span></a></li>
						<li><a href="#">D. <span>Zaire</span></a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="field-required next-pre">
            <div class="form-group submit-left">
                <button type="submit" id="btn-prev" class="btn-valider" style="display: inline-block;"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Prev</button>
            </div>
            <div class="form-group submit-right">
                <button type="submit" id="btn-next" class="btn-valider" style="display: block;">Next</button>
            </div> 
        </div>
	</div>
	
	<!-- end funny question -->

	<div class="most-read">
		<div class="title">
			tin mới nhất
		</div>
		<div id="news-latest">
			<div class="control">
                <a class="prev" href="#prev"><i class="zmdi zmdi-long-arrow-left"></i></a>
                <a class="next" href="#next"><i class="zmdi zmdi-long-arrow-right"></i></a>
            </div><!-- .control -->
            <div class="list-item-news-news-latest owl-carousel">
               	<div class="item">
               		<div class="item-news-news-latest">
               			<a href="#">
               				<div class="image">
               					<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/img-related-post.jpg')">
                                    <img src="assets/img/img-related-post.jpg" alt="logo">
                                </div>
               				</div>
	               			<div class="content">
	               				Paradox is reality, one a part of the other. My work is designed to cause contemplation...
	               			</div>
	               		</a>
               		</div>

               		<div class="item-news-news-latest">
               			<a href="#">
               				<div class="image">
               					<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/img-related-post.jpg')">
                                    <img src="assets/img/img-related-post.jpg" alt="logo">
                                </div>
               				</div>
	               			<div class="content">
	               				Paradox is reality, one a part of the other. My work is designed to cause contemplation...
	               			</div>
	               		</a>
               		</div>

               		<div class="item-news-news-latest">
               			<a href="#">
               				<div class="image">
               					<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/img-related-post.jpg')">
                                    <img src="assets/img/img-related-post.jpg" alt="logo">
                                </div>
               				</div>
	               			<div class="content">
	               				Paradox is reality, one a part of the other. My work is designed to cause contemplation...
	               			</div>
	               		</a>
               		</div>

               		<div class="item-news-news-latest">
               			<a href="#">
               				<div class="image">
               					<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/img-related-post.jpg')">
                                    <img src="assets/img/img-related-post.jpg" alt="logo">
                                </div>
               				</div>
	               			<div class="content">
	               				Paradox is reality, one a part of the other. My work is designed to cause contemplation...
	               			</div>
	               		</a>
               		</div>
               	</div>

               	<div class="item">
               		<div class="item-news-news-latest">
               			<a href="#">
               				<div class="image">
               					<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/img-related-post.jpg')">
                                    <img src="assets/img/img-related-post.jpg" alt="logo">
                                </div>
               				</div>
	               			<div class="content">
	               				Paradox is reality, one a part of the other. My work is designed to cause contemplation...
	               			</div>
	               		</a>
               		</div>

               		<div class="item-news-news-latest">
               			<a href="#">
               				<div class="image">
               					<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/img-related-post.jpg')">
                                    <img src="assets/img/img-related-post.jpg" alt="logo">
                                </div>
               				</div>
	               			<div class="content">
	               				Paradox is reality, one a part of the other. My work is designed to cause contemplation...
	               			</div>
	               		</a>
               		</div>

               		<div class="item-news-news-latest">
               			<a href="#">
               				<div class="image">
               					<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/img-related-post.jpg')">
                                    <img src="assets/img/img-related-post.jpg" alt="logo">
                                </div>
               				</div>
	               			<div class="content">
	               				Paradox is reality, one a part of the other. My work is designed to cause contemplation...
	               			</div>
	               		</a>
               		</div>

               		<div class="item-news-news-latest">
               			<a href="#">
               				<div class="image">
               					<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/img-related-post.jpg')">
                                    <img src="assets/img/img-related-post.jpg" alt="logo">
                                </div>
               				</div>
	               			<div class="content">
	               				Paradox is reality, one a part of the other. My work is designed to cause contemplation...
	               			</div>
	               		</a>
               		</div>
               	</div>
            </div>
		</div>
	</div>
	<!-- end latest news -->

	<div class="most-read">
		<div class="title">
			Đọc nhiều nhất
		</div>
		<div id="news-most-read">
			<div class="control">
                <a class="prev" href="#prev"><i class="zmdi zmdi-long-arrow-left"></i></a>
                <a class="next" href="#next"><i class="zmdi zmdi-long-arrow-right"></i></a>
            </div><!-- .control -->
            <div class="list-item-news-most-read owl-carousel">
               	<div class="item">
               		<div class="item-news-most-read">
               			<a href="#">
	               			<div class="content">
	               				Paradox is reality, one a part of the other. My work is designed to cause contemplation...
	               			</div>
	               			<div class="view-share">
	               				<div class="view">
	               					<i class="fa fa-eye"></i> 123456
	               				</div>
	               				<div class="share">
	               					<i class="fa fa-share-alt"></i>
	               				</div>
	               			</div>
	               		</a>
               		</div>

               		<div class="item-news-most-read">
               			<a href="#">
	               			<div class="content">
	               				Paradox is reality, one a part of the other. My work is designed to cause contemplation...
	               			</div>
	               			<div class="view-share">
	               				<div class="view">
	               					<i class="fa fa-eye"></i> 123456
	               				</div>
	               				<div class="share">
	               					<i class="fa fa-share-alt"></i>
	               				</div>
	               			</div>
	               		</a>
               		</div>

               		<div class="item-news-most-read">
               			<a href="#">
	               			<div class="content">
	               				Paradox is reality, one a part of the other. My work is designed to cause contemplation...
	               			</div>
	               			<div class="view-share">
	               				<div class="view">
	               					<i class="fa fa-eye"></i> 123456
	               				</div>
	               				<div class="share">
	               					<i class="fa fa-share-alt"></i>
	               				</div>
	               			</div>
	               		</a>
               		</div>

               		<div class="item-news-most-read">
               			<a href="#">
	               			<div class="content">
	               				Paradox is reality, one a part of the other. My work is designed to cause contemplation...
	               			</div>
	               			<div class="view-share">
	               				<div class="view">
	               					<i class="fa fa-eye"></i> 123456
	               				</div>
	               				<div class="share">
	               					<i class="fa fa-share-alt"></i>
	               				</div>
	               			</div>
	               		</a>
               		</div>
               	</div>

               	<div class="item">
               		<div class="item-news-most-read">
               			<a href="#">
	               			<div class="content">
	               				Paradox is reality, one a part of the other. My work is designed to cause contemplation...
	               			</div>
	               			<div class="view-share">
	               				<div class="view">
	               					<i class="fa fa-eye"></i> 123456
	               				</div>
	               				<div class="share">
	               					<i class="fa fa-share-alt"></i>
	               				</div>
	               			</div>
	               		</a>
               		</div>

               		<div class="item-news-most-read">
               			<a href="#">
	               			<div class="content">
	               				Paradox is reality, one a part of the other. My work is designed to cause contemplation...
	               			</div>
	               			<div class="view-share">
	               				<div class="view">
	               					<i class="fa fa-eye"></i> 123456
	               				</div>
	               				<div class="share">
	               					<i class="fa fa-share-alt"></i>
	               				</div>
	               			</div>
	               		</a>
               		</div>

               		<div class="item-news-most-read">
               			<a href="#">
	               			<div class="content">
	               				Paradox is reality, one a part of the other. My work is designed to cause contemplation...
	               			</div>
	               			<div class="view-share">
	               				<div class="view">
	               					<i class="fa fa-eye"></i> 123456
	               				</div>
	               				<div class="share">
	               					<i class="fa fa-share-alt"></i>
	               				</div>
	               			</div>
	               		</a>
               		</div>

               		<div class="item-news-most-read">
               			<a href="#">
	               			<div class="content">
	               				Paradox is reality, one a part of the other. My work is designed to cause contemplation...
	               			</div>
	               			<div class="view-share">
	               				<div class="view">
	               					<i class="fa fa-eye"></i> 123456
	               				</div>
	               				<div class="share">
	               					<i class="fa fa-share-alt"></i>
	               				</div>
	               			</div>
	               		</a>
               		</div>
               	</div>
            </div>
		</div>
	</div>
	<!-- end most-read -->
	<div class="advertise native">
		<a href="#">
			<img src="assets/img/sidebar/advertise-secc.jpg" alt="advertise" title="advertise">
		</a>
	</div>
</div>