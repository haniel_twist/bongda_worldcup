<!-- header -->
<section id="wrap-main">
    <div class="container">
        <div class="row row-0">
            <div class="col-lg-3 col-md-2 col-sm-4 col-xs-3">
                <div id="logo" class="logo-page">
                    <a href="#">
                        <img src="assets/img/logo-page.svg" alt="logo" title="logo">
                    </a>
                </div>
            </div>
            <div class="col-lg-9 col-md-10 col-sm-8 col-xs-9">
                <div class="wrap-main-right">
                    <div id="menu">
                        <?php require_once 'menu-page.php';?>
                    </div>
                    <div class="api-load fb-api">
                        <i class="fa fa-facebook" aria-hidden="true"></i>
                        <span>119k</span>
                    </div>
                    <div class="api-load yt-api">
                        <i class="fa fa-youtube-play" aria-hidden="true"></i>
                        <span>119k</span>
                    </div>
                    <div class="search-form">
                        <div class="icon-search">
                            <a href="#">
                                <i class="fa fa-search"></i>
                                <i class="fa fa-times" aria-hidden="true"></i>
                            </a>
                            
                        </div>
                        <form id="max-search" class="search" style="display: none;">
                            <input type="text" class="txt" name="" value="" placeholder="Tìm kiếm" />
                            <button class="but"><i class="fa fa-search"></i></button>
                        </form><!-- #max-search -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>




