<!-- header -->

<div class="header">
    <section id="score-live">
        <section class="container">
            <div class="row row-0">
                <div class="list-score-live">
                    <div class="control">
                        <a class="prev" href="#prev"></a>
                        <a class="next" href="#next"></a>
                    </div><!-- .control -->
                    <div class="list-item owl-carousel">
                        <div class="live-item">
                            <div class="row">
                                <div class="col-lg-4 col-xs-4">
                                    <div class="team team-left">
                                        <div class="image">
                                            <div class="super-img loaded ratio-1x1" style="background-image: url('assets/img/logo-mu.png')">
                                                <img src="assets/img/logo-mu.png" alt="logo">
                                            </div>
                                        </div>
                                        <p>Manchester United</p>
                                    </div>
                                </div> 
                                <div class="col-lg-4 col-xs-4">
                                    <div class="team score">
                                        <div class="date">
                                            6/1
                                        </div>
                                        <div class="time">
                                            3:00 AM WIB
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-xs-4">
                                    <div class="team team-right">
                                        <div class="image">
                                            <div class="super-img loaded ratio-1x1" style="background-image: url('assets/img/logo-derby.png')">
                                                <img src="assets/img/logo-derby.png" alt="ava">
                                            </div>
                                        </div>
                                        <p>Derby County</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="live-item">
                            <div class="row">
                                <div class="col-lg-4 col-xs-4">
                                    <div class="team team-left">
                                        <div class="image">
                                            <div class="super-img loaded ratio-1x1" style="background-image: url('assets/img/logo-derby.png')">
                                                <img src="assets/img/logo-derby.png" alt="logo">
                                            </div>
                                        </div>
                                        <p>Manchester United</p>
                                    </div>
                                </div> 
                                <div class="col-lg-4 col-xs-4">
                                    <div class="team score">
                                        <div class="date">
                                            6/1
                                        </div>
                                        <div class="time">
                                            3:00 AM WIB
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-xs-4">
                                    <div class="team team-right">
                                        <div class="image">
                                            <div class="super-img loaded ratio-1x1" style="background-image: url('assets/img/logo-mu.png')">
                                                <img src="assets/img/logo-mu.png" alt="ava">
                                            </div>
                                        </div>
                                        <p>Derby County</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="coutdown">
                        <div class="logo-worldcup">
                            <img src="assets/img/logo-worldcup.svg" alt="">
                        </div>
                        <div class="title">worldcup 2018</div>
                        <div id="timer">
                            <div id="getcountdown"></div>
                        </div>
                    </div>
                    
                </div>
                
            </div>
            
        </section>
    </section>
</div>

<section id="wrap-main">
    <div class="container">
        <div class="row row-0">
            <div class="col-lg-3 col-md-2 col-xs-3">
                <div id="logo">
                    <img src="assets/img/logo.svg" alt="logo" title="logo">
                    <a href="" class="worldcup-logo">
                         <img src="assets/img/logo-worldcup.png" alt="logo-worldcup" title="world-cup-2018">
                    </a>
                    <a href="/" class="bongda-logo">
                         <img src="assets/img/logo-bongda.svg" alt="logo-bongda" title="bongda.com.vn">
                    </a>
                </div>
            </div>
            <div class="col-lg-9 col-md-10 col-xs-9 ">
                <div class="wrap-main-right">
                    <div id="menu">
                        <?php require_once 'menu-page.php';?>
                    </div>
                    <div class="api-load fb-api">
                        <i class="fa fa-facebook" aria-hidden="true"></i>
                        <span>119k</span>
                    </div>
                    <div class="api-load yt-api">
                        <i class="fa fa-youtube-play" aria-hidden="true"></i>
                        <span>119k</span>
                    </div>
                    <div class="search-form">
                        <div class="icon-search">
                            <a href="">
                                <i class="fa fa-search"></i>
                                <i class="fa fa-times" aria-hidden="true"></i>
                            </a>
                            
                        </div>
                        <form id="max-search" class="search" style="">
                            <input type="text" class="txt" name="" value="" placeholder="Tìm kiếm" />
                            <button class="but"><i class="fa fa-search"></i></button>
                        </form><!-- #max-search -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>




