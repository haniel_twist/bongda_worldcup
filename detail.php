<!-- page detail -->
<div class="page wrap-main-content">
	<section class="container">
		
		<nav class="breadcrumb list-breadcrumb">
		  	<a class="breadcrumb-item" href="#"><i class="fa fa-home"></i></a>
		  	<a class="breadcrumb-item" href="#">Mới nhất</a>
		  	<span class="breadcrumb-item active">Tất tần tật các sao Bundesliga có thể góp mặt tại World Cup</span>
		</nav>
		<!-- breadcrumb -->

		<div class="row row-0">
			<div class="col-lg-2">
				<div class="advertise-left">
					<div class="advertise">
						<a href="#">
							<img src="assets/img/alowwatch.jpg" alt="advertise" title="advertise">
						</a>
					</div>
					<div class="advertise">
						<a href="#">
							<img src="assets/img/adam.jpg" alt="advertise" title="advertise">
						</a>
					</div>
				</div>		
			</div> 
			<!-- end col-lg-2 -->

			<div class="col-lg-7">
				<div class="main-content main-content-page">
					<div class="title">
						Tất tần tật các sao Bundesliga có thể góp mặt tại World Cup 
					</div>
					<div class="view-detail">
						<div class="view-left">
							<span class="time">4 hours ago</span>
							<span class="views"><i class="fa fa-eye"></i> 1,282 Views </span>
							<span class="comment"><i class="zmdi zmdi-comment-text-alt"></i> No Comments</span>
						</div>
						<div class="like-share">
							<a href="#">
								<img src="assets/img/btn-like-share.png" alt="">
							</a>
						</div>
					</div>
					<div class="sapo_detail">
						Phần 2 là những cầu thủ thuộc 8 quốc gia: Pháp, Đức, Nhật Bản, Ma-rốc, Nigeria, Iceland, Peru, Mexico
					</div>
					<figure class="expNoEdit">
						<img src="http://media.bongda.com.vn/files/thanhdat.to/2018/04/23/2018-04-22_234537-0825.png" alt="5 điểm nhấn Chelsea 2-0 Southampton - Bóng Đá" width="600" height="384">  
						<figcaption>  
							<h2 class="expEdit"> Corentin Tolisso và Benjamin Pavard trong màu áo tuyển Pháp.</h2>  
						</figcaption>  
					</figure>
					<div class="detail-content">
						<p>Tỏa sáng trong màu áo Stuttgart, Benjamin Pavard đã lọt vào mắt xanh của Didier Deschamps. Anh là một trong những nhân tố mới của Les Blues cho chiến dịch World Cup. Pavard đã có 2 lần khoác áo đội tuyển và tiếp tục được triệu tập lần này. Ngoài ra, Kingsley Coman và Corentin Tolisso như thường lệ vẫn có một suất trong đội hình tuyển Pháp.</p>
						<div class="related-post-content">
							<div class="item">
								<a href="#">
									Buffon trở lại tuyển Italia để lấy lại những gì đã mất
								</a>
								<p><span><i class="zmdi zmdi-time-restore"></i></span> 4 hours ago</p>
							</div>

							<div class="item">
								<a href="#">
									Maroc sẽ chi 16 tỷ USD cho World Cup 2026 nếu được chọn làm chủ nhà
								</a>
								<p><span><i class="zmdi zmdi-time-restore"></i></span> 4 hours ago</p>
							</div>
						</div>
						<p>Trên đây là một số cầu thủ nổi bật, vẫn có rất nhiều cái tên khác có thể góp mặt trong danh sách tuyển Đức tại nước Nga. Quả thật, Joachim Low là HLV 'đau đầu' nhất thế giới, ông có quá nhiều lựa chọn cho Cỗ xe tăng. Trang chủ Bundesliga từng đưa ra đến 4 đội hình tuyển Đức có thể mang sang xứ Bạch Dương, không ít trong số đó đang thi đấu ở trong nước.</p>
						<figure class="expNoEdit">
							<img src="http://media.bongda.com.vn/files/thanhdat.to/2018/04/23/2018-04-22_234537-0825.png" alt="5 điểm nhấn Chelsea 2-0 Southampton - Bóng Đá" width="600" height="384">  
							<figcaption>  
								<h2 class="expEdit">Corentin Tolisso và Benjamin Pavard trong màu áo tuyển Pháp.</h2>  
							</figcaption>  
						</figure>
						<p>Iceland tiếp tục làm nên điều kỳ diệu sau Euro 2016, họ trở thành đất nước nhỏ bé nhất từng tham dự một vòng chung kết World Cup. Finnbogason góp công không nhỏ trong chiến tích này với 3 bàn thắng vòng loại. Hiện tại, ở Bundesliga, anh đang đứng thứ 4 trong danh sách Vua phá lưới với 11 bàn thắng.</p>
						<p><strong>MEXICO: Marco Fabian, Carlos Salcedo (Frankfurt)</strong></p>
						<p>Frankfurt là đội bóng duy nhất sở hữu các cầu thủ đến từ Mexico tại Bundesliga. Họ đều là những cầu thủ hàng đầu lúc này của tuyển Mexico. 'Nhạc trưởng' Frankfurt, Marco Fabian trở lại sau chấn thương đang nỗ lực hết sức để lấy lại phong độ cao nhất.</p>
						<div class="new_relation_post">
							<a href="#">
								Buffon trở lại tuyển Italia để lấy lại những gì đã mất
							</a>
						</div>
						<p>Iceland tiếp tục làm nên điều kỳ diệu sau Euro 2016, họ trở thành đất nước nhỏ bé nhất từng tham dự một vòng chung kết World Cup. Finnbogason góp công không nhỏ trong chiến tích này với 3 bàn thắng vòng loại. Hiện tại, ở Bundesliga, anh đang đứng thứ 4 trong danh sách Vua phá lưới với 11 bàn thắng. Iceland tiếp tục làm nên điều kỳ diệu sau Euro 2016, họ trở thành đất nước nhỏ bé nhất từng tham dự một vòng chung kết World Cup. Finnbogason góp công không nhỏ trong chiến tích này với 3 bàn thắng vòng loại. Hiện tại, ở Bundesliga, anh đang đứng thứ 4 trong danh sách Vua phá lưới với 11 bàn thắng.</p>
					</div>
					

					<div class="social-bottom">
						<a href="#">
							<img src="assets/img/btn-like-share.png" alt="">
						</a>
					</div>
					<div class="list-tag">
						<div class="title">Xu hướng</div>
						<div class="name-tag">
							<a href="#"><span>#</span>LiverpoolLiverpool</a>
							<a href="#"><span>#</span>Live</a>
							<a href="#"><span>#</span>Liverpool</a>
							<a href="#"><span>#</span>Liverpool</a>
							<!-- <a href="#"><span>#</span>Liverpool</a> -->
						</div>
					</div>

					<!-- related post -->

					<div class="related related-post">
						<div class="title">Tin liên quan</div>
						<div class="list-related-post">
							<div id="related-post">
								<div class="control">
					                <a class="prev" href="#prev"><i class="zmdi zmdi-long-arrow-left"></i></a>
	                				<a class="next" href="#next"><i class="zmdi zmdi-long-arrow-right"></i></a>
					            </div><!-- .control -->
					            <div class="list-item-news-related-post owl-carousel">
					               	<div class="item">
					               		<a href="#">
						               		<div class="image">
						               			<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/img-related-post.jpg')">
		                                            <img src="assets/img/img-related-post.jpg" alt="logo">
		                                        </div>
						               		</div>
						               		<div class="content">
						               			<h3>Điêu đứng với sở thích "sống ảo" mùa chuyển nhượng của sao bóng đá</h3>
						               		</div>
						               	</a>
					               	</div>
					               	<div class="item">
					               		<a href="#">
						               		<div class="image">
						               			<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/img-related-post-2.jpg')">
		                                            <img src="assets/img/img-related-post-2.jpg" alt="logo">
		                                        </div>
						               		</div>
						               		<div class="content">
							               		<h3>Cách ngôi vô địch 1 trận thắng, Guardiola cất lời xem thường Cha...</h3>
							               	</div>
						               	</a>
					               	</div>

					               	<div class="item">
					               		<a href="#">
						               		<div class="image">
						               			<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/img-related-1.jpg')">
		                                            <img src="assets/img/img-related-1.jpg" alt="logo">
		                                        </div>
						               		</div>
						               		<div class="content">
							               		<h3>Điêu đứng với sở thích "sống ảo" mùa chuyển nhượng của sao bóng đá</h3>
							               	</div>
						               	</a>
					               	</div>

					               	<!-- <div class="item">
					               		<a href="#">
						               		<div class="image">
						               			<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/costa.jpg')">
		                                            <img src="assets/img/sidebar/costa.jpg" alt="logo">
		                                        </div>
						               		</div>
						               		<div class="content">
						               			<h3>Điêu đứng với sở thích "sống ảo" mùa chuyển nhượng của sao bóng đá</h3>
						               		</div>
						               	</a>
					               	</div> -->
					            </div>
							</div>
						</div>
					</div>

					<div class="related latest-news-post">
						<div class="title">Tin mới nhất</div>
						<div class="latest-news-post">
							<div id="news-latest-post">
								<div class="control">
					                <a class="prev" href="#prev"><i class="zmdi zmdi-long-arrow-left"></i></a>
	                				<a class="next" href="#next"><i class="zmdi zmdi-long-arrow-right"></i></a>
					            </div><!-- .control -->
					            <div class="list-item-latest-news-post owl-carousel">
					               	<div class="item">
					               		<a href="#">
						               		<div class="image">
						               			<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/img-related-post.jpg')">
		                                            <img src="assets/img/img-related-post.jpg" alt="logo">
		                                        </div>
						               		</div>
						               		<div class="content">
						               			<h3>Bị đá khỏi Champions League, Barca gây sốc với...</h3>
						               		</div>
						               	</a>
					               	</div>
					               	<div class="item">
					               		<a href="#">
						               		<div class="image">
						               			<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/img-related-post-2.jpg')">
		                                            <img src="assets/img/img-related-post-2.jpg" alt="logo">
		                                        </div>
						               		</div>
						               		<div class="content">
							               		<h3>Cách ngôi vô địch 1 trận thắng, Guardiola cất lời xem thường Cha...</h3>
							               	</div>
						               	</a>
					               	</div>

					               	<div class="item">
					               		<a href="#">
						               		<div class="image">
						               			<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/img-related-1.jpg')">
		                                            <img src="assets/img/img-related-1.jpg" alt="logo">
		                                        </div>
						               		</div>
						               		<div class="content">
							               		<h3>Điêu đứng với sở thích "sống ảo" mùa chuyển nhượng của sao bóng đá</h3>
							               	</div>
						               	</a>
					               	</div>

					               	<div class="item">
					               		<a href="#">
						               		<div class="image">
						               			<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/img-related-1.jpg')">
		                                            <img src="assets/img/img-related-1.jpg" alt="logo">
		                                        </div>
						               		</div>
						               		<div class="content">
							               		<h3>Điêu đứng với sở thích "sống ảo" mùa chuyển nhượng của sao bóng đá</h3>
							               	</div>
						               	</a>
					               	</div>
					            </div>
							</div>
						</div>
					</div>
				</div>

				
			</div>
			<!-- end col-lg-7 -->

			<div class="col-lg-3">
				<?php require 'sidebar.php';?>
			</div>
		</div>
	</section>
</div>

