<!-- footer -->

<section id="footer-page">
	<section class="container">
        <div class="row row-0">
        	<div class="col-lg-6">
        		<div class="footer-left">
        			<p>Toàn bộ nội dung bài viết, ý kiến thành viên được kiểm duyệt, cung cấp và bảo trợ thông tin bởi <span>Báo Thể Thao Việt Nam – Cơ quan thuộc Tổng Cục Thể Dục Thể Thao.</span></p>
        			<p><span>TS. Nhà báo Võ Danh Hải - Trưởng chi nhánh Phía Nam</span></p>
        			<p>Chịu trách nhiệm nội dung: <span>Bà Bạch Thị Hà </span>- Phó Giám đốc Công ty Cổ phần Yêu Thể Thao</p>
        			<p>Chỉ đạo thực hiện: <span>Nguyễn Đức Vinh</span>, Giám đốc điều hành Cty CP Yêu Thể Thao</p>
        			<div class="logo">
        				<ul>
        					<li>
        						<a href="#">
	        						<img src="assets/img/logo-tin-the-thao.png" title="" alt="">
	        					</a>
	        				</li>
	        				<li>
        						<a href="#">
	        						<img src="assets/img/logo-vo-thuat.png" title="" alt="">
	        					</a>
	        				</li>
	        				<li>
        						<a href="#">
	        						<img src="assets/img/logo-bongda24h.png" title="" alt="">
	        					</a>
	        				</li>
	        				<li>
        						<a href="#">
	        						<img src="assets/img/logo-thehinh.png" title="" alt="">
	        					</a>
	        				</li>
	        				<li>
        						<a href="#">
	        						<img src="assets/img/logo-xe.png" title="" alt="">
	        					</a>
	        				</li>
	        				<li>
        						<a href="#">
	        						<img src="assets/img/logo-khoe-dep.png" title="" alt="">
	        					</a>
	        				</li>
        				</ul>
        			</div>
        		</div>
        	</div>

        	<div class="col-lg-5">
        		<div class="footer-right">
        			<p>
        				<span class="icon"><img src="assets/img/icon/icon-check.png" title="" alt=""></span>
        				<span>Địa chỉ: </span>02 Đinh Tiên Hoàng, Phường.Đa Kao, Q.1, TP.HCM.
        			</p>
    				<p>
    					<span class="left">
    						<span class="icon"><img src="assets/img/icon/icon-phone.png" title="" alt=""></span>
        					<span>Điện thoại: </span>(08) 3910-5017 
    					</span>
    					<span class="right">
    						<span class="icon"><img src="assets/img/icon/icon-fax.png" title="" alt=""></span>
        					<span>Fax: </span>08) 3910-5019.
    					</span>
    				</p>
    				<p>
    					<span class="icon"><img src="assets/img/icon/icon-qc.png" title="" alt=""></span>
        				<span>Quảng cáo: </span> 0909 74 64 28.
    				</p>
    				<p>
    					<span class="icon"><img src="assets/img/icon/icon-email.png" title="" alt=""></span>
        				<span>Liên hệ quảng cáo: </span>quangcao@bongda.com.vn
    				</p>
    				<p>
    					<span class="icon"><img src="assets/img/icon/icon-headphone.png" title="" alt=""></span>
        				<span>Toà soạn & hỗ trợ: </span> (08) 3910-5018.
    				</p>
    				<p>
    					<span class="icon"><img src="assets/img/icon/icon-qc.png" title="" alt=""></span>
        				<span>Email: </span>hotro@bongda.com.vn
    				</p>
    				
        		</div>
        	</div>
        </div>
    </section>
    <section class="footer-bot">
        <div id="back-to-top">
            <a href="#"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
        </div>
    	<section class="container">
    		<div class="row row-0">
    			<div class="col-lg-6 col-md-6 col-sm-12">
    				<p>Toàn bộ nội dung bài viết, ý kiến thành viên được kiểm duyệt, cung cấp và bảo trợ thông tin bởi Báo Thể Thao Việt Nam </p>
    			</div>
    			<div class="col-lg-6 col-md-6 col-sm-12">
    				<div class="menu-footer">
    					<ul>
    						<li><a href="#">Mới nhất</a></li>
    						<li><a href="#">Lịch sử 32 đội</a></li>
    						<li><a href="#">Lịch thi đấu</a></li>
    						<li><a href="#">Thống kê</a></li>
    						<li><a href="#">Bên lề</a></li>
    						<li><a href="#">Ảnh</a></li>
    						<li><a href="#">Video</a></li>
    						<li><a href="#">Dự đoán</a></li>
    						<li><a href="#">Đố vui</a></li>
    					</ul>
    				</div>
    			</div>
    		</div>
    	</section>
    </section>
</section>