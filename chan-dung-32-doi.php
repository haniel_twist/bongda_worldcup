<!-- page chan dung 32 doi -->
<div class="page wrap-main-content">
	<section class="container">
		<div class="advertise-top">
			<a href="#" title=""><img src="assets/img/home/advertise-tiki.jpg" alt="" title=""></a>
		</div>
		<nav class="breadcrumb list-breadcrumb">
		  	<a class="breadcrumb-item" href="#"><i class="fa fa-home"></i></a>
		  	<span class="breadcrumb-item active">Chân dung 32 đội</span>
		</nav>
		<!-- breadcrumb -->

		<div class="row row-0">
			<div class="col-lg-2">
				<div class="advertise-left">
					<div class="advertise">
						<a href="#">
							<img src="assets/img/alowwatch.jpg" alt="advertise" title="advertise">
						</a>
					</div>
					<div class="advertise">
						<a href="#">
							<img src="assets/img/adam.jpg" alt="advertise" title="advertise">
						</a>
					</div>
				</div>		
			</div> 
			<!-- end col-lg-2 -->

			<div class="col-lg-7">
				<div class="main-content main-content-page page-team-fooball">
					<div class="title">32 Đội bóng theo bảng đấu</div>

					<div class="table-team">
						<div class="table-name">A</div>
						<div class="list-team">
							<div class="team">
								<div class="image">
									<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/team/netherlands.jpg')">
                                        <img src="assets/img/home/team/netherlands.jpg" alt="">
                                    </div>
								</div>
								<p>NGA</p>
							</div>
							<div class="team">
								<div class="image">
									<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/news/img-latest.jpg')">
                                        <img src="assets/img/news/img-latest.jpg" alt="">
                                    </div>
								</div>
								<p>Ả RẬP XÊ ÚT</p>
							</div>
							<div class="team">
								<div class="image">
									<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/news/img-latest.jpg')">
                                        <img src="assets/img/news/img-latest.jpg" alt="">
                                    </div>
								</div>
								<p>AI CẬP</p>
							</div>
							<div class="team">
								<div class="image">
									<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/news/img-latest.jpg')">
                                        <img src="assets/img/news/img-latest.jpg" alt="">
                                    </div>
								</div>
								<p>URUGUAY</p>
							</div>
						</div>
					</div>

					<div class="table-team">
						<div class="table-name">B</div>
						<div class="list-team">
							<div class="team">
								<div class="image">
									<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/news/img-latest.jpg')">
                                        <img src="assets/img/news/img-latest.jpg" alt="">
                                    </div>
								</div>
								<p>Bồ Đào Nha</p>
							</div>
							<div class="team">
								<div class="image">
									<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/news/img-latest.jpg')">
                                        <img src="assets/img/news/img-latest.jpg" alt="">
                                    </div>
								</div>
								<p>Tây Ban Nha</p>
							</div>
							<div class="team">
								<div class="image">
									<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/news/img-latest.jpg')">
                                        <img src="assets/img/news/img-latest.jpg" alt="">
                                    </div>
								</div>
								<p>MOROCCO</p>
							</div>
							<div class="team">
								<div class="image">
									<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/news/img-latest.jpg')">
                                        <img src="assets/img/news/img-latest.jpg" alt="">
                                    </div>
								</div>
								<p>IRAN</p>
							</div>
						</div>
					</div>

					<div class="table-team">
						<div class="table-name">C</div>
						<div class="list-team">
							<div class="team">
								<div class="image">
									<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/news/img-latest.jpg')">
                                        <img src="assets/img/news/img-latest.jpg" alt="">
                                    </div>
								</div>
								<p>Bồ Đào Nha</p>
							</div>
							<div class="team">
								<div class="image">
									<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/news/img-latest.jpg')">
                                        <img src="assets/img/news/img-latest.jpg" alt="">
                                    </div>
								</div>
								<p>Tây Ban Nha</p>
							</div>
							<div class="team">
								<div class="image">
									<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/news/img-latest.jpg')">
                                        <img src="assets/img/news/img-latest.jpg" alt="">
                                    </div>
								</div>
								<p>MOROCCO</p>
							</div>
							<div class="team">
								<div class="image">
									<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/news/img-latest.jpg')">
                                        <img src="assets/img/news/img-latest.jpg" alt="">
                                    </div>
								</div>
								<p>IRAN</p>
							</div>
						</div>
					</div>
					
					<div class="table-team">
						<div class="table-name">D</div>
						<div class="list-team">
							<div class="team">
								<div class="image">
									<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/news/img-latest.jpg')">
                                        <img src="assets/img/news/img-latest.jpg" alt="">
                                    </div>
								</div>
								<p>Bồ Đào Nha</p>
							</div>
							<div class="team">
								<div class="image">
									<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/news/img-latest.jpg')">
                                        <img src="assets/img/news/img-latest.jpg" alt="">
                                    </div>
								</div>
								<p>Tây Ban Nha</p>
							</div>
							<div class="team">
								<div class="image">
									<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/news/img-latest.jpg')">
                                        <img src="assets/img/news/img-latest.jpg" alt="">
                                    </div>
								</div>
								<p>MOROCCO</p>
							</div>
							<div class="team">
								<div class="image">
									<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/news/img-latest.jpg')">
                                        <img src="assets/img/news/img-latest.jpg" alt="">
                                    </div>
								</div>
								<p>IRAN</p>
							</div>
						</div>
					</div>

				</div>
			</div>
			<!-- end col-lg-7 -->

			<div class="col-lg-3">
				<?php require 'sidebar.php';?>
			</div>
		</div>
	</section>
</div>

