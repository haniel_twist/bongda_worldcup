<!-- page video -->
<div class="page wrap-main-content">
	<section class="container">
		<div class="advertise-top">
			<a href="#" title=""><img src="assets/img/home/advertise-tiki.jpg" alt="" title=""></a>
		</div>
		<nav class="breadcrumb list-breadcrumb">
		  	<a class="breadcrumb-item" href="#"><i class="fa fa-home"></i></a>
		  	<span class="breadcrumb-item active">Video</span>
		</nav>
		<!-- breadcrumb -->

		<div class="row row-0">
			<div class="col-lg-2">
				<div class="advertise-left">
					<div class="advertise">
						<a href="#">
							<img src="assets/img/alowwatch.jpg" alt="advertise" title="advertise">
						</a>
					</div>
					<div class="advertise">
						<a href="#">
							<img src="assets/img/adam.jpg" alt="advertise" title="advertise">
						</a>
					</div>
				</div>		
			</div> 
			<!-- end col-lg-2 -->

			<div class="col-lg-7">
				<div class="main-content main-content-page page-ben-le page-photo page-video">
					<div class="box-page-photo box-page-video">
						<div class="latest-photo latest-video">
							<a href="#">
								<div class="image">
									<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/news/img-latest.jpg')">
	                                    <img src="assets/img/news/img-latest.jpg" alt="">
	                                </div>
	                                <div class="btn-play">
	                                	<i class="zmdi zmdi-play"></i> Play
	                                </div>
								</div>
								
								<div class="content">
									<h3>Muốn "chuồn" sang Trung Quốc, Claudecir gây áp lực với Quảng Nam FC 2018</h3>
								</div>
							</a>
						</div>
						<div class="list-photo list-video">
							<div class="item">
								<a href="#">
									<div class="image">
										<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/news/img-latest.jpg')">
		                                    <img src="assets/img/news/img-latest.jpg" alt="">
		                                </div>
									</div>
									<div class="content">
										<h3>Muốn "chuồn" sang Trung Quốc, Claudecir gây áp lực với Quảng Nam FC 2018</h3>
									</div>
								</a>
							</div>
							<div class="item">
								<a href="#">
									<div class="image">
										<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/news/img-latest.jpg')">
		                                    <img src="assets/img/news/img-latest.jpg" alt="">
		                                </div>
									</div>
									<div class="content">
										<h3>Muốn "chuồn" sang Trung Quốc, Claudecir gây áp lực với Quảng Nam FC 2018</h3>
									</div>
								</a>
							</div>
							<div class="item">
								<a href="#">
									<div class="image">
										<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/news/img-latest.jpg')">
		                                    <img src="assets/img/news/img-latest.jpg" alt="">
		                                </div>
									</div>
									<div class="content">
										<h3>Muốn "chuồn" sang Trung Quốc, Claudecir gây áp lực với Quảng Nam FC 2018</h3>
									</div>
								</a>
							</div>
						</div>
					</div>

					<div class="item-news wrap-news-item">
						<a href="#">
							<div class="content-left">
								<div class="image">
									<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/news/img-latest.jpg')">
	                                    <img src="assets/img/news/img-latest.jpg" alt="">
	                                </div>
								</div>
							</div>
							<div class="content-right">
								<h3>Nhìn lại những khoảnh khắc khiến tuyển Anh phải ‘độn thổ’ (Phần 2)</h3>
								<span class="border-title"></span>
								<p>Bên cạnh những thành công bao giờ cũng có những điểm tối. Ta sẽ cùng nhìn lại những khoảnh khắc khiến Tam Sư phải muối mặt nhất.</p>
							</div>
						</a>
					</div>

					<div class="item-news wrap-news-item">
						<a href="#">
							<div class="content-left">
								<div class="image">
									<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/news/img-latest.jpg')">
	                                    <img src="assets/img/news/img-latest.jpg" alt="">
	                                </div>
								</div>
							</div>
							<div class="content-right">
								<h3>Nhìn lại những khoảnh khắc khiến tuyển Anh phải ‘độn thổ’ (Phần 2)</h3>
								<span class="border-title"></span>
								<p>Bên cạnh những thành công bao giờ cũng có những điểm tối. Ta sẽ cùng nhìn lại những khoảnh khắc khiến Tam Sư phải muối mặt nhất.</p>
							</div>
						</a>
					</div>

					<div class="item-news wrap-news-item">
						<a href="#">
							<div class="content-left">
								<div class="image">
									<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/news/img-latest.jpg')">
	                                    <img src="assets/img/news/img-latest.jpg" alt="">
	                                </div>
								</div>
							</div>
							<div class="content-right">
								<h3>Nhìn lại những khoảnh khắc khiến tuyển Anh phải ‘độn thổ’ (Phần 2)</h3>
								<span class="border-title"></span>
								<p>Bên cạnh những thành công bao giờ cũng có những điểm tối. Ta sẽ cùng nhìn lại những khoảnh khắc khiến Tam Sư phải muối mặt nhất.</p>
							</div>
						</a>
					</div>

					<div class="item-news wrap-news-item">
						<a href="#">
							<div class="content-left">
								<div class="image">
									<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/news/img-latest.jpg')">
	                                    <img src="assets/img/news/img-latest.jpg" alt="">
	                                </div>
								</div>
							</div>
							<div class="content-right">
								<h3>Nhìn lại những khoảnh khắc khiến tuyển Anh phải ‘độn thổ’ (Phần 2)</h3>
								<span class="border-title"></span>
								<p>Bên cạnh những thành công bao giờ cũng có những điểm tối. Ta sẽ cùng nhìn lại những khoảnh khắc khiến Tam Sư phải muối mặt nhất.</p>
							</div>
						</a>
					</div>

					<div id="paging">
						<ul>
							<li><a href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li class="active"><a href="#">4</a></li>
							<li><a href="#">5</a></li>
							<li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>
						</ul>
					</div>

				</div>
			</div>
			<!-- end col-lg-7 -->

			<div class="col-lg-3">
				<?php require 'sidebar.php';?>
			</div>
		</div>
	</section>
</div>

