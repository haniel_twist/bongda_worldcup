<!-- page do vui -->
<div class="page wrap-main-content">
	<section class="container">
		<div class="advertise-top">
			<a href="#" title=""><img src="assets/img/home/advertise-tiki.jpg" alt="" title=""></a>
		</div>
		<nav class="breadcrumb list-breadcrumb">
		  	<a class="breadcrumb-item" href="#"><i class="fa fa-home"></i></a>
		  	<span class="breadcrumb-item active">Đố vui</span>
		</nav>
		<!-- breadcrumb -->

		<div class="row row-0">
			<div class="col-lg-2">
				<div class="advertise-left">
					<div class="advertise">
						<a href="#">
							<img src="assets/img/alowwatch.jpg" alt="advertise" title="advertise">
						</a>
					</div>
					<div class="advertise">
						<a href="#">
							<img src="assets/img/adam.jpg" alt="advertise" title="advertise">
						</a>
					</div>
				</div>		
			</div> 
			<!-- end col-lg-2 -->

			<div class="col-lg-7">
				<div class="main-content main-content-page page-questions">
					<div class="banner">
						<img src="assets/img/banner-do-vui.jpg" alt="advertise" title="advertise">
					</div>
					<div class="item-questions">
						<div class="title">
							Câu hỏi 1
						</div>
						<div class="question">
							Đội tuyển nào sau đây chỉ mới có được một lần duy nhất vinh dự tranh tài tại vòng chung kết World Cup?
						</div>
						<div class="answer">
							<ul>
								<li><a href="#"><span>A.</span> Brazil</a></li>
								<li><a href="#"><span>B.</span> Togo</a></li>
								<li><a href="#"><span>C.</span> Croatia</a></li>
								<li><a href="#"><span>D.</span> Zaire</a></li>
							</ul>
						</div>
					</div>
					<div class="item-questions">
						<div class="title">
							Câu hỏi 2
						</div>
						<div class="question">
							Đội tuyển nào sau đây chỉ mới có được một lần duy nhất vinh dự tranh tài tại vòng chung kết World Cup?
						</div>
						<div class="answer">
							<ul>
								<li><a href="#"><span>A.</span> Brazil</a></li>
								<li><a href="#"><span>B.</span> Togo</a></li>
								<li><a href="#"><span>C.</span> Croatia</a></li>
								<li><a href="#"><span>D.</span> Zaire</a></li>
							</ul>
						</div>
					</div>
					<div class="item-questions">
						<div class="title">
							Câu hỏi 3
						</div>
						<div class="question">
							Đội tuyển nào sau đây chỉ mới có được một lần duy nhất vinh dự tranh tài tại vòng chung kết World Cup?
						</div>
						<div class="answer">
							<ul>
								<li><a href="#"><span>A.</span> Brazil</a></li>
								<li><a href="#"><span>B.</span> Togo</a></li>
								<li><a href="#"><span>C.</span> Croatia</a></li>
								<li><a href="#"><span>D.</span> Zaire</a></li>
							</ul>
						</div>
					</div>
					<div class="item-questions">
						<div class="title">
							Câu hỏi 4
						</div>
						<div class="question">
							Đội tuyển nào sau đây chỉ mới có được một lần duy nhất vinh dự tranh tài tại vòng chung kết World Cup?
						</div>
						<div class="answer">
							<ul>
								<li><a href="#"><span>A.</span> Brazil</a></li>
								<li><a href="#"><span>B.</span> Togo</a></li>
								<li><a href="#"><span>C.</span> Croatia</a></li>
								<li><a href="#"><span>D.</span> Zaire</a></li>
							</ul>
						</div>
					</div>
					<div class="item-questions">
						<div class="title">
							Câu hỏi 5
						</div>
						<div class="question">
							Đội tuyển nào sau đây chỉ mới có được một lần duy nhất vinh dự tranh tài tại vòng chung kết World Cup?
						</div>
						<div class="answer">
							<ul>
								<li><a href="#"><span>A.</span> Brazil</a></li>
								<li><a href="#"><span>B.</span> Togo</a></li>
								<li><a href="#"><span>C.</span> Croatia</a></li>
								<li><a href="#"><span>D.</span> Zaire</a></li>
							</ul>
						</div>
					</div>					
				</div>
			</div>
			<!-- end col-lg-7 -->

			<div class="col-lg-3">
				<?php require 'sidebar.php';?>
			</div>
		</div>
	</section>
</div>

