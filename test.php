<!-- page tin moi nhat -->
<style>
.background_white {background: #FFF !important;}
table.act_default thead tr th {font-size: 11px; color: #999; font-weight: bold;}
.span12 form.form-search {
    background: #f0f6fa; 
    padding: 12px 14px;
}
.span12 form.form-search .portlet.box {
    margin-top: 18px;
    position: relative;
    display: inline-block;
    width: 20%;
    float: left;
} 
.span12 form.form-search .portlet.box .portlet-body .control-group{
    position: relative;
} 
.span12 form.form-search .portlet.box .portlet-body .control-group .controls{
    display: inline-block; 
    width: 100%
} 
.span12 form.form-search .portlet.box .portlet-body .control-group .radio-button{
    position: absolute; 
    top: 53%;
    right:0px
}
</style>
<div class="page wrap-main-content">
	
<div class="page-container row-fluid">
	<div class="page-content">
		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12">
					<h3 class="page-title">
						<!-- Tạo mới câu hỏi <small><?php echo $oneItem['title'] ?></small> -->
                        <a href="?mod=<?php echo $mod ?>" class="btn blue pull-right"><i class="icon-chevron-left"></i> Quay lại</a>
					</h3>
				</div>
			</div>

            <div class="row-fluid" style="font-family: Arial;">
				<div class="span12">
                    <div class="row-fluid">
						<form class="form-search" action="" method="post" style="background: #f0f6fa; padding: 12px 14px;">
                        <button type="button" class="btn_save btn blue "><i class="icon-ok"></i> Save </button>
                        <button type="button" class="btn_add btn red "><i class="icon-plus"></i> Thêm </button>
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption"> Nhóm câu hỏi 1</div>
                                    <div style=""><a class="btn_insert" style="font-size:18px;color:red" ><i class="fa fa-home"></i></a></div>
                                </div>
                                <div class="portlet-body">
                                    <div class="control-group">
                                        <label class="control-label" for="f_tags">Câu hỏi</label>
                                        <div class="controls">
                                            <input name="dovui[1][question]" type="text" id="question" placeholder="Nhập câu hỏi" class="m-wrap span12" />
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="f_tags">Câu trả lời 1</label>
                                        <div class="controls" style="">
                                            <input name="dovui[1][answer][]" type="text" id="answer" class="m-wrap span12" placeholder="Nhập câu trả lời"  /> 
                                        </div>
                                        <label class="radio-button">
                                             <input name="kq1" value="1" type="radio" />
                                        </label>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="f_tags">Câu trả lời 2</label>
                                        <div class="controls">
                                            <input name="dovui[1][answer][]" type="text" id="answer" class="m-wrap span12" placeholder="Nhập câu trả lời"  /> 
                                        </div>
                                        <label class="radio-button">
                                             <input name="kq1" value="1" type="radio" />
                                        </label>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="f_tags">Câu trả lời 3</label>
                                        <div class="controls">
                                            <input name="dovui[1][answer][]" type="text" id="answer" class="m-wrap span12" placeholder="Nhập câu trả lời"  /> 
                                        </div>
                                        <label class="radio-button">
                                             <input name="kq1" value="1" type="radio" />
                                        </label>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="f_tags">Câu trả lời 4</label>
                                        <div class="controls">
                                            <input name="dovui[1][answer][]" type="text" id="answer" class="m-wrap span12" placeholder="Nhập câu trả lời"  /> 
                                        </div>
                                        <label class="radio-button">
                                             <input name="kq1" value="1" type="radio" />
                                        </label>
                                    </div>
                                </div>
                            </div>
                            
                            <div id="box_FQ">
                            </div>   
                        <button type="button" class="btn_save btn blue "><i class="icon-ok"></i> Save </button>
                        <button type="button" class="btn_add btn red "><i class="icon-plus"></i> Thêm </button>
						</form>
					</div>

					<div class="portlet-append" style="display: none">
						<div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption"> Nhóm câu hỏi 1</div>
                                <div style=""><a class="btn_insert" style="font-size:18px;color:red" ><i class="fa fa-home"></i></a></div>
                            </div>
                            <div class="portlet-body">
                                <div class="control-group">
                                    <label class="control-label" for="f_tags">Câu hỏi</label>
                                    <div class="controls">
                                        <input name="dovui[1][question]" type="text" id="question" placeholder="Nhập câu hỏi" class="m-wrap span12" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="f_tags">Câu trả lời </label>
                                    <div class="controls" style="">
                                        <input name="dovui[1][answer][]" type="text" id="answer" class="m-wrap span12" placeholder="Nhập câu trả lời"  /> 
                                    </div>
                                    <label class="radio-button">
                                         <input name="kq1" value="1" type="radio" />
                                    </label>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="f_tags">Câu trả lời 2</label>
                                    <div class="controls">
                                        <input name="dovui[1][answer][]" type="text" id="answer" class="m-wrap span12" placeholder="Nhập câu trả lời"  /> 
                                    </div>
                                    <label class="radio-button">
                                         <input name="kq1" value="1" type="radio" />
                                    </label>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="f_tags">Câu trả lời 3</label>
                                    <div class="controls">
                                        <input name="dovui[1][answer][]" type="text" id="answer" class="m-wrap span12" placeholder="Nhập câu trả lời"  /> 
                                    </div>
                                    <label class="radio-button">
                                         <input name="kq1" value="1" type="radio" />
                                    </label>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="f_tags">Câu trả lời 4</label>
                                    <div class="controls">
                                        <input name="dovui[1][answer][]" type="text" id="answer" class="m-wrap span12" placeholder="Nhập câu trả lời"  /> 
                                    </div>
                                    <label class="radio-button">
                                         <input name="kq1" value="1" type="radio" />
                                    </label>
                                </div>
                            </div>
                        </div>
					</div>
				</div>
			</div>

			<!-- END PAGE CONTENT-->
		</div>
		<!-- END PAGE CONTAINER-->
	</div>
	<!-- END PAGE -->
</div>

</div>

