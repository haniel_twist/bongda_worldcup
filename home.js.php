<script type="text/javascript">
    $(document).ready(function() {

        $('#score-live .list-item').owlCarousel({
            loop: true,
            margin: 36,
            autoplay: false,
            autoplayTimeout: 8000,
            slideBy: 1,
            items: 1
        });
        // -- Control -- 
        var listRoll = $("#score-live .list-item").owlCarousel();
        // Next
        $('#score-live .next').click(function(){
            listRoll.trigger('next.owl.carousel');
            return false;
        });
        // Prev
        $('#score-live .prev').click(function(){
            listRoll.trigger('prev.owl.carousel');
            return false;
        });


        $('#slide-hot .list-item-hot').owlCarousel({
            loop: true,
            margin: 36,
            autoplay: false,
            autoplayTimeout: 8000,
            slideBy: 1,
            // animateOut: 'fadeOut',
            items: 1
        });
        // -- Control -- 
        var listRoll1 = $("#slide-hot .list-item-hot").owlCarousel();
        // Next
        $('#slide-hot .next').click(function(){
            listRoll1.trigger('next.owl.carousel');
            return false;
        });
        // Prev
        $('#slide-hot .prev').click(function(){
            listRoll1.trigger('prev.owl.carousel');
            return false;
        });


        $('#slider-statistic .list-item-statistic').owlCarousel({
            loop: true,
            margin: 36,
            autoplay: false,
            autoplayTimeout: 8000,
            slideBy: 1,
            items: 1
            // animateOut: 'fadeOut',
        });
        // -- Control -- 
        var listRoll2 = $("#slider-statistic .list-item-statistic").owlCarousel();
        // Next
        $('#slider-statistic .next').click(function(){
            listRoll2.trigger('next.owl.carousel');
            return false;
        });
        // Prev
        $('#slider-statistic .prev').click(function(){
            listRoll2.trigger('prev.owl.carousel');
            return false;
        });

        //slider box picture
        $('#slider-picture .list-item-picture').on('initialized.owl.carousel changed.owl.carousel', function(e) {
            if (!e.namespace)  {
              return;
            }
            var carousel = e.relatedTarget;
            $('.slider-counter').text(carousel.relative(carousel.current()) + 1 + '/' + carousel.items().length);
        });
        $('#slider-picture .list-item-picture').owlCarousel({
            loop: true,
            margin: 36,
            autoplay: false,
            autoplayTimeout: 8000,
            slideBy: 1,
            items: 1
        });
        // -- Control -- 
        var listRoll3 = $("#slider-picture .list-item-picture").owlCarousel();
        // Next
        $('#slider-picture .next').click(function(){
            listRoll3.trigger('next.owl.carousel');
            return false;
        });
        // Prev
        $('#slider-picture .prev').click(function(){
            listRoll3.trigger('prev.owl.carousel');
            return false;
        });
        

        // $(window).on('load resize', function() {
        //     var statistic_right = $('.statistic-right').height();
        //     $('.statistic-left').css('height', statistic_right + 1);
        // });

        $('.box-btn-info .show-image').click(function() {
            $('#show-image-slider').addClass('show-all');
            // console.log('sas');
        });
        $('.btn-close-show').click(function() {
            $('#show-image-slider').removeClass('show-all');
            // console.log('sas');
        });


        // get countdown
        var countDownDate = new Date("May 23, 2018").getTime();

        // Update the count down every 1 second
        var x = setInterval(function() {

            // Get todays date and time
            var now = new Date().getTime();
            
            // Find the distance between now an the count down date
            var distance = countDownDate - now;
            
            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            if (days < "10") { days = "0" + days; }
            if (hours < "10") { hours = "0" + hours; }
            if (minutes < "10") { minutes = "0" + minutes; }
            if (seconds < "10") { seconds = "0" + seconds; }
            
            // Output the result in an element with id="demo"
            document.getElementById("getcountdown").innerHTML = days + "" + hours + ""
            + minutes + "" + seconds + "";
            
            // If the count down is over, write some text 
            if (distance < 0) {
                clearInterval(x);
                document.getElementById("getcountdown").innerHTML = "EXPIRED";
            }
            // string coutdown
            if(document.getElementById("getcountdown") != null){
                var abc = document.getElementById('getcountdown').innerHTML;

                var stringHTML = '<ul>';
                for(var i = 0; i < abc.length; i ++){
                    var char = abc[i];
                    stringHTML += '<li>' + char + '</li>';
                }

                stringHTML += '</ul>';

                document.getElementById('getcountdown').innerHTML = stringHTML;
            }
        }, 1000);

    });
    
</script>
