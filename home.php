<!-- home page -->
<div class="advertise-top">
	<a href="#" title=""><img src="assets/img/home/advertise-tiki.jpg" alt="" title=""></a>
</div>
<div class="wrap-main-content">
	<section class="container">
		<div class="main-section">
			<div class="row row-0">
				<div class="col-lg-9">
					<div class="hot-box">
						<div class="hot-box-left">
							<div class="schedul">
								<h2>Lịch thi đấu</h2>
								<div class="list-item-schedul">
									<div class="item">
										<div class="title">
											<h3>Group A</h3>
											<p><span>14 Jun 2018</span> - <span>18:00</span></p>
										</div>
										<div class="group-item">
											<div class="group-1">
												<div class="ensign">
													<div class="super-img loaded ratio-3x2" style="background-image: url('assets/img/home/br.jpg')">
		                                                <img src="assets/img/home/br.jpg" alt="logo">
		                                            </div>
												</div>
												<p>BRA</p>
											</div>

											<div class="group-2">
												<div class="ensign">
													<div class="super-img loaded ratio-3x2" style="background-image: url('assets/img/home/costa.jpg')">
		                                                <img src="assets/img/home/costa.jpg" alt="logo">
		                                            </div>
												</div>
												<p>Costa Costa</p>
											</div>
										</div>
									</div>

									<div class="item">
										<div class="title">
											<h3>Group A</h3>
											<p><span>14 Jun 2018</span> - <span>18:00</span></p>
										</div>
										<div class="group-item">
											<div class="group-1">
												<div class="ensign">
													<div class="super-img loaded ratio-3x2" style="background-image: url('assets/img/home/br.jpg')">
		                                                <img src="assets/img/home/br.jpg" alt="logo">
		                                            </div>
												</div>
												<p>BRA</p>
											</div>

											<div class="group-2">
												<div class="ensign">
													<div class="super-img loaded ratio-3x2" style="background-image: url('assets/img/home/costa.jpg')">
		                                                <img src="assets/img/home/costa.jpg" alt="logo">
		                                            </div>
												</div>
												<p>Costa</p>
											</div>
										</div>
									</div>

									<div class="item">
										<div class="title">
											<h3>Group A</h3>
											<p><span>14 Jun 2018</span> - <span>18:00</span></p>
										</div>
										<div class="group-item">
											<div class="group-1">
												<div class="ensign">
													<div class="super-img loaded ratio-3x2" style="background-image: url('assets/img/home/br.jpg')">
		                                                <img src="assets/img/home/br.jpg" alt="logo">
		                                            </div>
												</div>
												<p>BRA</p>
											</div>

											<div class="group-2">
												<div class="ensign">
													<div class="super-img loaded ratio-3x2" style="background-image: url('assets/img/home/costa.jpg')">
		                                                <img src="assets/img/home/costa.jpg" alt="logo">
		                                            </div>
												</div>
												<p>Costa</p>
											</div>
										</div>
									</div>	
									
									<div class="view-all">
										<a href="#" title="">Xem tất cả</a>
									</div>
								</div>
							</div>
						</div>
						<!-- end hot box left -->

						<div class="hot-box-center">
							<div id="slide-hot">
								<div class="control">
			                        <a class="prev" href="#prev"><i class="zmdi zmdi-chevron-left"></i></a>
			                        <a class="next" href="#next"><i class="zmdi zmdi-chevron-right"></i></a>
			                    </div><!-- .control -->
			                    <div class="list-item-hot owl-carousel">
			                       	<div class="item-hot">
			                       		<div class="image">
			                       			<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/img-hot.jpg')">
                                                <img src="assets/img/home/img-hot.jpg" alt="logo">
                                            </div>
			                       		</div>
			                       		<div class="content">
			                       			<h3>10 ngôi sao nhiều khả năng không được dự World Cup 10 ngôi sao nhiều khả năng không được dự World Cup 10 ngôi sao nhiều khả năng không được dự World Cup</h3>
			                       			<p>Đội tuyển quốc gia giành quyền tham dự World Cup 2018, nhưng nhiều ngôi sao vì nhiều lý do có thể sẽ phải ở nhà chứng kiến đồng đội thi đấu.</p>
			                       		</div>
			                       	</div>
			                       	<div class="item-hot">
			                       		<div class="image">
			                       			<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/img-hot.jpg')">
                                                <img src="assets/img/home/img-hot.jpg" alt="logo">
                                            </div>
			                       		</div>
			                       		<div class="content">
			                       			<h3>10 ngôi sao nhiều khả năng không được dự World Cup</h3>
			                       			<p>Đội tuyển quốc gia giành quyền tham dự World Cup 2018, nhưng nhiều ngôi sao vì nhiều lý do có thể sẽ phải ở nhà chứng kiến đồng đội thi đấu.</p>
			                       		</div>
			                       	</div>
			                    </div>
							</div>
						</div>
						<!-- end hot box center -->

						<div class="hot-box-right">
							<div class="news-hot">
								<h2>Nóng nhất</h2>
								<div class="list-news-hot">
									<div class="item-news-hot">
										<a href="#">
											<div class="image">
												<div class="super-img loaded ratio-8x5" style="background-image: url('assets/img/home/img-hot.jpg')">
	                                                <img src="assets/img/home/img-hot.jpg" alt="logo">
	                                            </div>
											</div>
											<div class="content">
												<div class="title">
													Muốn bằng Maradona, Messi phải vô địch World...
												</div>
												<div class="detail">
													<span class="date">15/06/18</span>
													<span class="view"><i class="fa fa-eye"></i> 123456</span>
													<span class="share"><i class="fa fa-share-alt"></i></span>
												</div>
											</div>
										</a>
									</div>
									<div class="item-news-hot">
										<a href="#">
											<div class="image">
												<div class="super-img loaded ratio-8x5" style="background-image: url('assets/img/home/img-hot.jpg')">
	                                                <img src="assets/img/home/img-hot.jpg" alt="logo">
	                                            </div>
											</div>
											<div class="content">
												<div class="title">
													10 ngôi sao nhiều khả năng không được dự World Cup
												</div>
												<div class="detail">
													<span class="date">15/06/18</span>
													<span class="view"><i class="fa fa-eye"></i> 123456</span>
													<span class="share"><i class="fa fa-share-alt"></i></span>
												</div>
											</div>
										</a>
									</div>
									<div class="item-news-hot">
										<a href="#">
											<div class="image">
												<div class="super-img loaded ratio-8x5" style="background-image: url('assets/img/home/img-hot.jpg')">
	                                                <img src="assets/img/home/img-hot.jpg" alt="logo">
	                                            </div>
											</div>
											<div class="content">
												<div class="title">
													Muốn bằng Maradona, Messi phải vô địch World...
												</div>
												<div class="detail">
													<span class="date">15/06/18</span>
													<span class="view"><i class="fa fa-eye"></i> 123456</span>
													<span class="share"><i class="fa fa-share-alt"></i></span>
												</div>
											</div>
										</a>
									</div>
									<div class="item-news-hot">
										<a href="#">
											<div class="image">
												<div class="super-img loaded ratio-8x5" style="background-image: url('assets/img/home/img-hot.jpg')">
	                                                <img src="assets/img/home/img-hot.jpg" alt="logo">
	                                            </div>
											</div>
											<div class="content">
												<div class="title">
													10 ngôi sao nhiều khả năng không được dự World Cup
												</div>
												<div class="detail">
													<span class="date">15/06/18</span>
													<span class="view"><i class="fa fa-eye"></i> 123456</span>
													<span class="share"><i class="fa fa-share-alt"></i></span>
												</div>
											</div>
										</a>
									</div>
									<div class="item-news-hot">
										<a href="#">
											<div class="image">
												<div class="super-img loaded ratio-8x5" style="background-image: url('assets/img/home/img-hot.jpg')">
	                                                <img src="assets/img/home/img-hot.jpg" alt="logo">
	                                            </div>
											</div>
											<div class="content">
												<div class="title">
													10 ngôi sao nhiều khả năng không được dự World Cup
												</div>
												<div class="detail">
													<span class="date">15/06/18</span>
													<span class="view"><i class="fa fa-eye"></i> 123456</span>
													<span class="share"><i class="fa fa-share-alt"></i></span>
												</div>
											</div>
										</a>
									</div>
								</div>

							</div>
						</div>
						<!-- end hot box right -->
					</div>
					<!-- end hot box -->

					<div class="guess-result">
						<div class="box-guess-result">
							<div class="item-left">
								<div class="logo-group">
                                    <img src="assets/img/home/logo-real.png" alt="logo">
								</div>
								<div class="name-group">
									Real Madrid
								</div>
							</div>

							<div class="item-center">
								<h2>DỰ ĐOÁN TỶ SỐ</h2>
								<div class="tournament">
									WORLD CUP 2018
								</div>
								<div class="date-time">
									<div class="time">
										<span>2:45</span>,
										<span>16/06</span>
									</div>
									
								</div>
							</div>

							<div class="item-right">
								<div class="logo-group">
                                    <img src="assets/img/logo-derby.png" alt="logo">
								</div>
								<div class="name-group">
									Real Madrid
								</div>
							</div>
						</div>
					</div>
					<!-- end guess result -->

					<div class="news-latest box-white">
						<div class="title title-border-left">
							<h2>Mới nhất</h2>
						</div>
						<div class="list-news-latest">
							<div class="news-left">
								<div class="item-list-news">
									<a href="#">
										<div class="title">
											NÓNG: Vì Mourinho, trò cũ sẵn sàng 'lật kèo' PSG
										</div>
										<div class="content">
											Lorem ipsum dolor sit amet, consectetur adipisicing lit,por consectetur adipisicing lit,por...
										</div>
									</a>
								</div>
								<div class="item-list-news">
									<a href="#">
										<div class="title">
											Arsenal gây SỐC: Thay Sanchez bằng sao Dortmund
										</div>
										<div class="content">
											Lorem ipsum dolor sit amet, consectetur adipisicing lit,por consectetur adipisicing lit,por...
										</div>
									</a>
								</div>
								<div class="item-list-news">
									<a href="#">
										<div class="title">
											Marcos Alonso, hậu vệ có hiệu suất ghi bàn tốt nhất Premier League?
										</div>
										<div class="content">
											Lorem ipsum dolor sit amet, consectetur adipisicing lit,por consectetur adipisicing lit,por...
										</div>
									</a>
								</div>
							</div>

							<div class="news-center">
								<a href="#">
									<div class="image">
										<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/img-news-latest.jpg')">
	                                        <img src="assets/img/home/img-news-latest.jpg" alt="logo">
	                                    </div>
									</div>
									<div class="content">
										Chiếc cúp vàng danh giá trở lại nước Đức
									</div>
								</a>	
							</div>

							<div class="news-right">
								<div class="item-list-news">
									<a href="#">
										<div class="title">
											NÓNG: Vì Mourinho, trò cũ sẵn sàng 'lật kèo' PSG
										</div>
										<div class="content">
											Lorem ipsum dolor sit amet, consectetur adipisicing lit,por consectetur adipisicing lit,por...
										</div>
									</a>
								</div>
								<div class="item-list-news">
									<a href="#">
										<div class="title">
											Arsenal gây SỐC: Thay Sanchez bằng sao Dortmund
										</div>
										<div class="content">
											Lorem ipsum dolor sit amet, consectetur adipisicing lit,por consectetur adipisicing lit,por...
										</div>
									</a>
								</div>
								<div class="item-list-news">
									<a href="#">
										<div class="title">
											Marcos Alonso, hậu vệ có hiệu suất ghi bàn tốt nhất Premier League?
										</div>
										<div class="content">
											Lorem ipsum dolor sit amet, consectetur adipisicing lit,por consectetur adipisicing lit,por...
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
					<!-- end news latest -->

					<div class="team-history box-white">
						<div class="title title-border-left">
							<h2>Lịch sử 32 đội</h2>
							<div class="view-more">
								<a href="#">Xem tất cả <i class="fa fa-long-arrow-alt-right"></i></a>
							</div>
						</div>

						<div class="list-team">
							<div class="item-team">
								<a href="#">
									<div class="image">
										<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/team/brazil.jpg')">
	                                        <img src="assets/img/home/team/brazil.jpg" alt="logo">
	                                    </div>
									</div>
									<p>Brazil</p>
								</a>
							</div>

							<div class="item-team">
								<a href="#">
									<div class="image">
										<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/team/mexico.jpg')">
	                                        <img src="assets/img/home/team/mexico.jpg" alt="logo">
	                                    </div>
									</div>
									<p>Mexico</p>
								</a>
							</div>

							<div class="item-team">
								<a href="#">
									<div class="image">
										<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/team/croatia.jpg')">
	                                        <img src="assets/img/home/team/croatia.jpg" alt="logo">
	                                    </div>
									</div>
									<p>Croatia</p>
								</a>
							</div>

							<div class="item-team">
								<a href="#">
									<div class="image">
										<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/team/cameroon.jpg')">
	                                        <img src="assets/img/home/team/cameroon.jpg" alt="logo">
	                                    </div>
									</div>
									<p>Cameroon</p>
								</a>
							</div>

							<div class="item-team">
								<a href="#">
									<div class="image">
										<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/team/netherlands.jpg')">
	                                        <img src="assets/img/home/team/netherlands.jpg" alt="logo">
	                                    </div>
									</div>
									<p>Netherlands</p>
								</a>
							</div>

							<div class="item-team">
								<a href="#">
									<div class="image">
										<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/team/chile.jpg')">
	                                        <img src="assets/img/home/team/chile.jpg" alt="logo">
	                                    </div>
									</div>
									<p>Chile</p>
								</a>
							</div>

							<div class="item-team">
								<a href="#">
									<div class="image">
										<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/team/spain.jpg')">
	                                        <img src="assets/img/home/team/spain.jpg" alt="logo">
	                                    </div>
									</div>
									<p>Spain</p>
								</a>
							</div>

							<div class="item-team">
								<a href="#">
									<div class="image">
										<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/team/aus.jpg')">
	                                        <img src="assets/img/home/team/aus.jpg" alt="logo">
	                                    </div>
									</div>
									<p>Australia</p>
								</a>
							</div>

							<div class="item-team">
								<a href="#">
									<div class="image">
										<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/team/colombia.jpg')">
	                                        <img src="assets/img/home/team/colombia.jpg" alt="logo">
	                                    </div>
									</div>
									<p>Colombia</p>
								</a>
							</div>

							<div class="item-team">
								<a href="#">
									<div class="image">
										<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/team/greece.jpg')">
	                                        <img src="assets/img/home/team/greece.jpg" alt="logo">
	                                    </div>
									</div>
									<p>Greece</p>
								</a>
							</div>

							<div class="item-team">
								<a href="#">
									<div class="image">
										<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/team/cote.jpg')">
	                                        <img src="assets/img/home/team/cote.jpg" alt="logo">
	                                    </div>
									</div>
									<p>Côte d'Ivoire</p>
								</a>
							</div>

							<div class="item-team">
								<a href="#">
									<div class="image">
										<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/team/japan.jpg')">
	                                        <img src="assets/img/home/team/japan.jpg" alt="logo">
	                                    </div>
									</div>
									<p>Japan</p>
								</a>
							</div>

							<div class="item-team">
								<a href="#">
									<div class="image">
										<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/team/costarica.jpg')">
	                                        <img src="assets/img/home/team/costarica.jpg" alt="logo">
	                                    </div>
									</div>
									<p>Costa Rica</p>
								</a>
							</div>

							<div class="item-team">
								<a href="#">
									<div class="image">
										<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/team/uruguay.jpg')">
	                                        <img src="assets/img/home/team/uruguay.jpg" alt="logo">
	                                    </div>
									</div>
									<p>Uruguay</p>
								</a>
							</div>

							<div class="item-team">
								<a href="#">
									<div class="image">
										<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/team/italy.jpg')">
	                                        <img src="assets/img/home/team/italy.jpg" alt="logo">
	                                    </div>
									</div>
									<p>Italy</p>
								</a>
							</div>

							<div class="item-team">
								<a href="#">
									<div class="image">
										<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/team/england.jpg')">
	                                        <img src="assets/img/home/team/england.jpg" alt="logo">
	                                    </div>
									</div>
									<p>England</p>
								</a>
							</div>

							<div class="item-team">
								<a href="#">
									<div class="image">
										<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/team/france.jpg')">
	                                        <img src="assets/img/home/team/france.jpg" alt="logo">
	                                    </div>
									</div>
									<p>France</p>
								</a>
							</div>

							<div class="item-team">
								<a href="#">
									<div class="image">
										<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/team/switzerland.jpg')">
	                                        <img src="assets/img/home/team/switzerland.jpg" alt="logo">
	                                    </div>
									</div>
									<p>Switzerland</p>
								</a>
							</div>
							
						</div>
					</div>
					<!-- end team history -->

					<div class="statistic">
						<div class="statistic-left">
							<div class="title">
								Thống kê
							</div>
							<div class="content">
								<a href="#">
									<h3>Những thống kê “không thể ngờ” sau VL World Cup 2018</h3>
									<p>Trải qua vòng loại World Cup 2018, FIFA đã thống kê ra những con số khiến những Fan của môn thể thao Vua không thể ngờ tới.</p>
								</a>
							</div>
						</div>
						<div class="statistic-right">
							<div id="slider-statistic">
								<div class="control">
			                        <a class="prev" href="#prev"><i class="zmdi zmdi-chevron-left"></i></a>
			                        <a class="next" href="#next"><i class="zmdi zmdi-chevron-right"></i></a>
			                    </div><!-- .control -->
			                    <div class="list-item-statistic owl-carousel">
			                       	<div class="item-statistic">
			                       		<div class="image">
			                       			<div class="super-img loaded ratio-2x1" style="background-image: url('assets/img/home/img-statistic.jpg')">
                                                <img src="assets/img/home/img-statistic.jpg" alt="logo">
                                            </div>
			                       		</div>
			                       		<div class="content">
			                       			<h3>Tổng số bàn thắng tại Vòng loại World Cup 2018 lên tới 2.416 bàn thắng.</h3>
			                       		</div>
			                       	</div>
			                       	<div class="item-statistic">
			                       		<div class="image">
			                       			<div class="super-img loaded ratio-2x1" style="background-image: url('assets/img/home/img-statistic.jpg')">
                                                <img src="assets/img/home/img-statistic.jpg" alt="logo">
                                            </div>
			                       		</div>
			                       		<div class="content">
			                       			<h3>Tổng số bàn thắng tại Vòng loại World Cup 2018 lên tới 2.416 bàn thắng.</h3>
			                       		</div>
			                       	</div>
			                    </div>
							</div>
						</div>
					</div>
					<!-- end statistic -->

					<div class="beside box-white">
						<div class="title title-border-left">
							<h2>Bên lề</h2>
						</div>
						<div class="box-beside">
							<div class="box-beside-left">
								<div class="item">
									<a href="#">
										<div class="image">
											<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/img-news-latest.jpg')">
		                                        <img src="assets/img/home/img-news-latest.jpg" alt="logo">
		                                    </div>
										</div>
										<div class="content">
											<h3>Inter Milan dọn đường cho Man Utd đón Mesut Ozil</h3>
											<p>Arthur Melo, sao trẻ 21 tuổi người Brazil mới đây đã có những chia sẻ thẳng thắn về mối liên hệ giữa anh và ban lãnh đạo Barcelona.</p>
										</div>
									</a>
								</div>
								<div class="item item-red-color">
									<a href="#">
										<div class="image">
											<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/img-news-latest.jpg')">
		                                        <img src="assets/img/home/img-news-latest.jpg" alt="logo">
		                                    </div>
										</div>
										<div class="content">
											<h3>Công khai hút thuốc và nốc rượu, Nainggolan bị Roma trừng phạt</h3>
											<p>Bayern, Atletico tranh sao Ligue 1; Tương lai David Luiz chưa sáng tỏ; Pochettino cùng Pep đá xoáy Mourinho... </p>
										</div>
									</a>
								</div>
							</div>

							<div class="box-beside-right">
								<div class="item-box-beside">
									<a href="">
										Điểm tin chiều 16/12: Mourinho chào bán Mkhitaryan; Maradona công kích Ronaldo
									</a>
								</div>
								<div class="item-box-beside">
									<a href="">
										Điểm tin tối 15/12: Man Utd bỏ Ozil, mua Willian
									</a>
								</div>
								<div class="item-box-beside">
									<a href="">
										Nóng: Liverpool chọn được sao Real thay Coutinho
									</a>
								</div>
								<div class="item-box-beside">
									<a href="">
										Điểm tin chiều 16/12: Mourinho chào bán Mkhitaryan; Maradona công kích Ronaldo
									</a>
								</div>
								<div class="item-box-beside">
									<a href="">
										Điểm tin chiều 16/12: Mourinho chào bán Mkhitaryan; Maradona công kích Ronaldo
									</a>
								</div>
							</div>
						</div>
					</div>
					<!-- end beside -->

					<div class="picture box-white">
						<div class="title title-border-left">
							<h2>Ảnh</h2>
						</div>
						<div class="box-picture">
							<div class="box-picture-left">
								<div id="slider-picture">
									<div class="control">
				                        <a class="prev" href="#prev"><i class="zmdi zmdi-chevron-left"></i></a>
			                        	<a class="next" href="#next"><i class="zmdi zmdi-chevron-right"></i></a>
				                    </div><!-- .control -->
				                    <div class="list-item-picture owl-carousel">
				                       	<div class="item-picture">
				                       		<a href="#">
					                       		<div class="image">
					                       			<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/img-picture-left.jpg')">
		                                                <img src="assets/img/home/img-picture-left.jpg" alt="logo">
		                                            </div>
					                       		</div>
					                       		<div class="content">
					                       			<h3>Cenk Tosun - Tân binh 27 triệu bảng của Everton có gì đặc biệt?</h3>
					                       		</div>
					                       	</a>
				                       	</div>
				                       	<div class="item-picture">
				                       		<a href="">
					                       		<div class="image">
					                       			<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/img-picture-left.jpg')">
		                                                <img src="assets/img/home/img-picture-left.jpg" alt="logo">
		                                            </div>
					                       		</div>
					                       		<div class="content">
					                       			<h3>Cenk Tosun - Tân binh 27 triệu bảng của Everton có gì đặc biệt?</h3>
					                       		</div>
					                       	</a>
					                    </div>
				                       	<div class="item-picture">
				                       		<a href="">
					                       		<div class="image">
					                       			<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/img-picture-left.jpg')">
		                                                <img src="assets/img/home/img-picture-left.jpg" alt="logo">
		                                            </div>
					                       		</div>
					                       		<div class="content">
					                       			<h3>Cenk Tosun - Tân binh 27 triệu bảng của Everton có gì đặc biệt?</h3>
					                       		</div>
					                       	</a>
					                    </div>
				                    </div>
				                    <div class="box-btn-info">
				                    	<div class="slider-counter"></div>
				                    	<div class="show-image"><i class="zmdi zmdi-apps"></i></div>
				                    	<div class="share"><a href="#"><i class="zmdi zmdi-share"></i></a></div>

				                    </div>
				                    <!-- end box info -->
									<div id="show-image-slider">
										<div class="btn-close-show">
											<i class="zmdi zmdi-close"></i>
										</div>
				                    	<div class="list-view-picture">
											<div class="item">
												<a href="#">
													<div class="image">
														<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/img-picture-left.jpg')">
					                                        <img src="assets/img/home/img-picture-left.jpg" alt="logo">
					                                    </div>
													</div>
													<p>Cenk Tosun - Tân binh 27 triệu bảng của Everton có gì đặc biệt?</p>
												</a>
											</div>

											<div class="item">
												<a href="#">
													<div class="image">
														<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/img-picture-right-1.jpg')">
					                                        <img src="assets/img/home/img-picture-right-1.jpg" alt="logo">
					                                    </div>
													</div>
													<p>Cenk Tosun - Tân binh 27 triệu bảng của Everton có gì đặc biệt?</p>
												</a>
											</div>

											<div class="item">
												<a href="#">
													<div class="image">
														<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/img-picture-right-2.jpg')">
					                                        <img src="assets/img/home/img-picture-right-2.jpg" alt="logo">
					                                    </div>
													</div>
													<p>Messi chọn ra 4 ứng cử viên hàng đầu cho... </p>
												</a>
											</div>
											<div class="item">
												<a href="#">
													<div class="image">
														<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/img-picture-right-3.jpg')">
					                                        <img src="assets/img/home/img-picture-right-3.jpg" alt="logo">
					                                    </div>
													</div>
													<p>Messi đập tan tin đồn đang có hiềm khích...</p>
												</a>
											</div>
											<div class="item">
												<a href="#">
													<div class="image">
														<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/img-picture-right-1.jpg')">
					                                        <img src="assets/img/home/img-picture-right-1.jpg" alt="logo">
					                                    </div>
													</div>
													<p>Messi chọn ra 4 ứng cử viên hàng đầu cho... </p>
												</a>
											</div>
											<div class="item">
												<a href="#">
													<div class="image">
														<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/img-picture-right-3.jpg')">
					                                        <img src="assets/img/home/img-picture-right-3.jpg" alt="logo">
					                                    </div>
													</div>
													<p>Messi đập tan tin đồn đang có hiềm khích...</p>
												</a>
											</div>
										</div>
				                    </div>
				                    
								</div>
							</div>
							<div class="box-picture-right">
								<div class="list-item-picture">
									<div class="item">
										<a href="#">
											<div class="image">
												<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/img-picture-left.jpg')">
			                                        <img src="assets/img/home/img-picture-left.jpg" alt="logo">
			                                    </div>
											</div>
											<p>Cenk Tosun - Tân binh 27 triệu bảng của Everton có gì đặc biệt?</p>
										</a>
									</div>

									<div class="item">
										<a href="#">
											<div class="image">
												<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/img-picture-right-1.jpg')">
			                                        <img src="assets/img/home/img-picture-right-1.jpg" alt="logo">
			                                    </div>
											</div>
											<p>Cenk Tosun - Tân binh 27 triệu bảng của Everton có gì đặc biệt?</p>
										</a>
									</div>

									<div class="item">
										<a href="#">
											<div class="image">
												<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/img-picture-right-2.jpg')">
			                                        <img src="assets/img/home/img-picture-right-2.jpg" alt="logo">
			                                    </div>
											</div>
											<p>Messi chọn ra 4 ứng cử viên hàng đầu cho... </p>
										</a>
									</div>
									<div class="item">
										<a href="#">
											<div class="image">
												<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/img-picture-right-3.jpg')">
			                                        <img src="assets/img/home/img-picture-right-3.jpg" alt="logo">
			                                    </div>
											</div>
											<p>Messi đập tan tin đồn đang có hiềm khích...</p>
										</a>
									</div>
									<div class="item">
										<a href="#">
											<div class="image">
												<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/img-picture-right-1.jpg')">
			                                        <img src="assets/img/home/img-picture-right-1.jpg" alt="logo">
			                                    </div>
											</div>
											<p>Messi chọn ra 4 ứng cử viên hàng đầu cho... </p>
										</a>
									</div>
									<div class="item">
										<a href="#">
											<div class="image">
												<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/img-picture-right-3.jpg')">
			                                        <img src="assets/img/home/img-picture-right-3.jpg" alt="logo">
			                                    </div>
											</div>
											<p>Messi đập tan tin đồn đang có hiềm khích...</p>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- end picture -->

					<div class="video box-white">
						<div class="box-item-video">
							<div class="item-video">
								<a href="#">
									<div class="image">
										<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/img-news-latest.jpg')">
	                                        <img src="assets/img/home/img-news-latest.jpg" alt="logo">
	                                    </div>
	                                    <div class="btn-play">
	                                    	<i class="zmdi zmdi-play"></i> Play
	                                    </div>
									</div>
									<div class="content">
										Muốn "chuồn" sang Trung Quốc, Claudecir gây áp lực với Quảng Nam FC
									</div>
								</a>
							</div>
							<div class="more-video">
								<a href="#">
									<i class="zmdi zmdi-plus"></i> More Video
								</a>
								
							</div>
						</div>
					</div>
					<!-- end video -->

					<div class="guess box-white">
						<div class="title title-border-left">
							<h2>Dự đoán</h2>
						</div>
						<div class="box-guess">
							<div class="box-guess-top">
								<a href="#">
									<div class="image">
										<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/img-news-latest.jpg')">
	                                        <img src="assets/img/home/img-news-latest.jpg" alt="logo">
	                                    </div>
									</div>
									<div class="content">
										<h3>Vua phá lưới World Cup 2018: Cơ hội nào cho Ronaldo, Messi?</h3>
										<p>Ronaldo và Messi đã có rất nhiều những danh hiệu cá nhân cao quý như Qủa Bóng Vàng, giày vàng châu Âu, cầu thủ xuất sắc nhất thế giới…. Nhưng Vua Phá lưới World Cup thì cả hai cá nhân kiệt xuất này vẫn chưa thể có được. Liệu rằng Messi hay Ronaldo sẽ là người có được danh hiệu cá nhân đó tại World Cup 2018 để hoàn tất bộ sưu tập khủng danh hiệu cá nhân của mình và bước vào ngôi đền của những huyền thoại thế giới?</p>
									</div>
								</a>
							</div>
							<div class="box-guess-bot">
								<div class="list-item-guess">
									<div class="item-guess">
										<a href="#">
											<div class="image">
												<div class="super-img loaded ratio-3x2" style="background-image: url('assets/img/home/item-guess-1.jpg')">
			                                        <img src="assets/img/home/item-guess-1.jpg" alt="logo">
			                                    </div>
											</div>
											<p>ĐT Đức tại World Cup 2018: Xứng danh nhà đương kim vô địch</p>
										</a>
									</div>

									<div class="item-guess">
										<a href="#">
											<div class="image">
												<div class="super-img loaded ratio-3x2" style="background-image: url('assets/img/home/item-guess-2.jpg')">
			                                        <img src="assets/img/home/item-guess-2.jpg" alt="logo">
			                                    </div>
											</div>
											<p>ĐT Brazil tại World Cup 2018: Thời khắc chín muồi</p>
										</a>
									</div>

									<div class="item-guess">
										<a href="#">
											<div class="image">
												<div class="super-img loaded ratio-3x2" style="background-image: url('assets/img/home/item-guess-1.jpg')">
			                                        <img src="assets/img/home/item-guess-1.jpg" alt="logo">
			                                    </div>
											</div>
											<p>ĐT Tây Ban Nha tại World Cup 2018: Tìm lại ánh hào quang</p>
										</a>
									</div>

									<div class="item-guess">
										<a href="#">
											<div class="image">
												<div class="super-img loaded ratio-3x2" style="background-image: url('assets/img/home/item-guess-2.jpg')">
			                                        <img src="assets/img/home/item-guess-2.jpg" alt="logo">
			                                    </div>
											</div>
											<p>Cục diện bảng H World Cup 2018: Nhật Bản khó có cửa đi tiếp</p>
										</a>
									</div>

									<div class="item-guess">
										<a href="#">
											<div class="image">
												<div class="super-img loaded ratio-3x2" style="background-image: url('assets/img/home/item-guess-1.jpg')">
			                                        <img src="assets/img/home/item-guess-1.jpg" alt="logo">
			                                    </div>
											</div>
											<p>Chùm ảnh: Bóng đá xứ Nghệ vỡ òa niềm vui vô địch tại "chảo lửa" thành Vinh</p>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- end col-lg-9 -->

				<div class="col-lg-3">
					<?php require 'sidebar.php';?>
				</div>
			</div>
		</div>
		
	</section>
	
</div>