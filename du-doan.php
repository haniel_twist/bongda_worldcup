<!-- page du doan -->
<div class="page wrap-main-content">
	<section class="container">
		<div class="advertise-top">
			<a href="#" title=""><img src="assets/img/home/advertise-tiki.jpg" alt="" title=""></a>
		</div>
		<nav class="breadcrumb list-breadcrumb">
		  	<a class="breadcrumb-item" href="#"><i class="fa fa-home"></i></a>
		  	<span class="breadcrumb-item active">Dự đoán</span>
		</nav>
		<!-- breadcrumb -->

		<div class="row row-0">
			<div class="col-lg-2">
				<div class="advertise-left">
					<div class="advertise">
						<a href="#">
							<img src="assets/img/alowwatch.jpg" alt="advertise" title="advertise">
						</a>
					</div>
					<div class="advertise">
						<a href="#">
							<img src="assets/img/adam.jpg" alt="advertise" title="advertise">
						</a>
					</div>
				</div>		
			</div> 
			<!-- end col-lg-2 -->

			<div class="col-lg-7">
				<div class="main-content main-content-page page-schedule page-guess">
					<ul id="tab-page"> 
					   <li class="active"><a href="#the-le">Thể lệ cuộc thi</a></li>
					   <li><a href="#du-doan">Dự đoán</a></li>
					</ul> 	

					<!-- content the le -->
					<div class="content" id="the-le">
						<h3>Thể lệ chương trình Dự đoán World Cup 2018 </h3>
						<p>Chương trình Dự đoán World Cup 2018 do báo điện tử phối hợp với nhãn hàng “CLEAR MEN - Đàn ông đỉnh cao phong độ” tổ chức nhân vòng chung kết bóng đá thế giới diễn ra tại Brazil. Chương trình bắt đầu từ 12h ngày 2/6/2018 đến 4h sáng 14/7/2018.</p>
						<h4>Điều 1: Người tham gia</h4>
						<ul>
							<li>- Các công dân Việt Nam và người nước ngoài đang sinh sống trên lãnh thổ Việt Nam đều có thể tham gia.</li>
							<li>- Cán bộ, nhân viên của và Nhà tài trợ không được tham gia chương trình này.</li>
						</ul>
						<h4>Điều 2: Các hình thức dự đoán</h4>
						<ul>
							<li>- Dự đoán tỷ số: Là dự đoán đúng tỷ số các trận đấu trong thời gian thi đấu chính thức (quy định là 90 phút, không tính kết quả sau hai hiệp phụ hay đá 11 mét). Ở mỗi trận đấu, người dự đoán chính xác tỷ số, đoán gần nhất tổng số người dự đoán và gửi tin nhắn sớm nhất là người chiến thắng.</li>
							<li>- Dự đoán đội vô địch: Là dự đoán đúng đội sẽ chiến thắng trong trận chung kết và giành Cup thế giới. Người dự đoán chính xác đội đoạt giải, đoán gần nhất số người dự đoán đúng kết quả và gửi tin nhắn sớm nhất là người chiến thắng.</li>
							<li>- Dự đoán Vua phá lưới: Là dự đoán đúng cầu thủ ghi được nhiều bàn thắng nhất giải đấu và được FIFA trao giải Vua phá lưới. Người dự đoán chính xác cầu thủ đoạt giải, đoán gần nhất số người dự đoán đúng kết quả và gửi tin nhắn sớm nhất là người chiến thắng.</li>
						</ul>
						<h4>Điều 3: Cách thức tham gia</h4>
						<ul>
							<li>Người chơi vào trang Dự đoán </li>
							<li>- <span>Bước 1:</span> Người chơi điền tỷ số (nếu tham gia Dự đoán tỷ số), chọn đội (nếu tham gia dự đoán Đội vô địch), chọn cầu thủ (nếu tham gia dự đoán Vua phá lưới).</li>
							<li>- <span>Bước 2:</span> Hệ thống trả ra bảng thông báo. Người chơi nhập tổng số lượng người dự đoán trên bảng này và bấm Gửi. Hệ thống trả ra mã xác nhận tương ứng và hướng dẫn cách nhắn tin xác nhận.</li>
							<li>- <span>Bước 3:</span> Người chơi nhắn tin với cú pháp và mã xác nhận được cung cấp.</li>
							<li>- <span>Bước 4:</span> Sau khi hết thời gian quy định cho mỗi vòng dự đoán, hệ thống sẽ kiểm tra danh sách người chơi dự đoán đúng, so sánh số dự đoán của mỗi người với tổng số người dự đoán đúng và sắp xếp theo thời gian gửi tin nhắn xác nhận để tìm ra người thắng cuộc.</li>
							<li>- <span>Bước 1:</span> Ban tổ chức kiểm tra tính hợp lệ của số điện thoại thắng cuộc và thông báo danh sách đoạt giải lên trang http</li>
						</ul>
						<p>Cú pháp nhắn tin dự đoán chung: <span>WC_[ma xac nhan] gửi 8500</span></p>
						<ul>
							<li>-	Trường hợp người chơi nhắn tin nhưng do lỗi hệ thống, lỗi đường truyền mà không nhận được tin nhắn trả về xác nhận thì tin nhắn đó được coi là “không thành công”.</li>
							<li>-   Trường hợp người chơi vi phạm quy định spam của nhà mạng (xem cụ thể tại đây) thì sẽ không được công nhận kết quả.</li>
							<li>-	Trường hợp người chơi lợi dụng quy định của các nhà mạng nhằm nhắn tin trục lợi, nếu Ban tổ chức phát hiện thì kết quả những tin nhắn đó sẽ bị huỷ bỏ.</li>
						</ul>
						<h4>Điều 4: Giải thưởng</h4>
						<ul>
							<li>- Dự đoán tỷ số: 64 giải dành cho 64 trận (mỗi trận đấu trao một giải). Mỗi giải là một thẻ điện thoại trị giá 500.000 đồng nạp trực tiếp vào tài khoản số điện thoại trúng giải.</li>
							<li>- Dự đoán Vua phá lưới: Người thắng cuộc nhận quà tặng là 01 chiếc iPhone 5S 32G (19.480.000đ).</li>
							<li>- Dự đoán Đội vô địch: Người thắng cuộc nhận quà tặng là 01 chiếc Macbook Air 13.3” - MD760ZP/B (2014) (23.990.000đ).</li>
							<li>Các quà tặng bằng sản phẩm sẽ không được quy đổi thành tiền mặt.</li>
						</ul>
					</div>

					<!-- content du doan -->
					<div class="content content-tab" id="du-doan">
						<div class="title">
							Lịch thi đấu
						</div>
						<div class="table-tab">
							<table class="table table-bordered"> 
						   		<tbody> 
						   			<tr> 
						   				<th scope="row">
						   					14/6/2018
						   				</th> 
						   				<td>22h00</td> 
						   				<td>A</td> 
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="form-input form-right">
				                                    	<input type="text" class="form-control" id="" placeholder="">
				                                    </div>
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													-
												</div>

						   						<div class="team team-right">
						   							<div class="form-input form-right">
				                                    	<input type="text" class="form-control" id="" placeholder="">
				                                    </div>
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/br.jpg')">
				                                            <img src="assets/img/sidebar/br.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Brazil</div>
						   						</div>
						   					</div>
						   					<div class="btn-submit">
		                                    	<button type="submit" class="btn btn-default">Gửi</button>
		                                    	<!-- Modal -->
												<div class="modal-mqc">
												    <div class="modal-content">
												      	<div class="modal-header">
												        	<h4 class="modal-title" id="">Mã xác nhận</h4>
												      	</div>
												      	<div class="modal-body">
												      		<h5>Mã xác nhận <span>sadsa</span></h5>
												       		<p>Soạn tin nhắn theo cú pháp <span class="mess">absc X Y</span> gửi <span class="send-number">1244</span></p>
												      	</div>
												      	<div class="modal-footer">
												        	<button type="button" class="btn btn-close btn-default">Đóng</button>
												      	</div>
												    </div>
												</div>
		                                    </div>
						   				</td> 
						   			</tr>  
						   			<tr> 
						   				<th scope="row">
						   					14/6/2018
						   				</th> 
						   				<td>22h00</td> 
						   				<td>A</td> 
						   				<td>
						   					<div class="team-match">
						   						<div class="team team-left">
						   							<div class="form-input form-right">
				                                    	<input type="text" class="form-control" id="" placeholder="">
				                                    </div>
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/spain.png')">
				                                            <img src="assets/img/sidebar/spain.png" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Nga</div>
						   						</div>
												
												<div class="team center">
													-
												</div>

						   						<div class="team team-right">
						   							<div class="form-input form-right">
				                                    	<input type="text" class="form-control" id="" placeholder="">
				                                    </div>
						   							<div class="image">
							   							<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/sidebar/br.jpg')">
				                                            <img src="assets/img/sidebar/br.jpg" alt="logo">
				                                        </div>
				                                    </div>
				                                    <div class="content">Saudi Arabia</div>
						   						</div>
						   					</div>
						   					<div class="btn-submit">
		                                    	<button type="submit" class="btn btn-default">Gửi</button>
		                                    	<!-- <div class="modal-mqc">
												    <div class="modal-content">
												      	<div class="modal-header">
												        	<h4 class="modal-title" id="">Mã xác nhận</h4>
												      	</div>
												      	<div class="modal-body">
												      		<h5>Mã xác nhận <span>sadsa</span></h5>
												       		<p>Soạn tin nhắn theo cú pháp <span class="mess">absc X Y</span> gửi <span class="send-number">1244</span></p>
												      	</div>
												      	<div class="modal-footer">
												        	<button type="button" class="btn btn-close btn-default">Đóng</button>
												      	</div>
												    </div>
												</div> -->
		                                    </div>
						   				</td> 
						   			</tr>  
						   		</tbody> 
						   	</table>
						</div>

						<div class="title">
							Vua phá lưới
						</div>
						<div class="table-tab">
							<div class="form-confirm">
								<!-- <form action=""> -->
									<div class="form-group">
										<label for="selectTeam">Chọn đội</label>
									    <select class="form-control">
										  	<option value="Duc">Đức</option>
										  	<option value="Brazil">Brazil</option>
										  	<option value="Phap">Pháp</option>
										  	<option value="Thuy-sy">Thuỵ Sỹ</option>
										  	<option value="Ba-lan">Ba Lan</option>
										  	<option value="Han-quoc">Hàn quốc</option>
										</select>
									</div>
									<div class="form-group">
										<label for="selectNam">Chọn cầu thủ</label>
									    <select class="form-control">
										  	<option value="A">A</option>
										  	<option value="B">B</option>
										  	<option value="C">C</option>
										  	<option value="D">D</option>
										  	<option value="E">E</option>
										  	<option value="F">F</option>
										</select>
									</div>
									<div class="form-group">
										<label for="total">Tổng số người dự đoán</label>
									    <input type="number" class="form-control" id="" placeholder="">
									</div>
									<div class="form-group button-submit btn-submit">
									    <button type="submit" class="btn btn-default">Xác nhận</button>
									    <div class="modal-mqc">
										    <div class="modal-content">
										      	<div class="modal-header">
										        	<h4 class="modal-title" id="">Mã xác nhận</h4>
										      	</div>
										      	<div class="modal-body">
										      		<h5>Mã xác nhận <span>sadsa</span></h5>
										       		<p>Soạn tin nhắn theo cú pháp <span class="mess">absc X Y</span> gửi <span class="send-number">1244</span></p>
										      	</div>
										      	<div class="modal-footer">
										        	<button type="button" class="btn btn-close btn-default">Đóng</button>
										      	</div>
										    </div>
										</div>
									</div>
								<!-- </form> -->
							</div>
						</div>
						
						<div class="title">
							Đội vu...
						</div>
						<div class="table-tab">
							<div class="form-confirm">
								<!-- <form action=""> -->
									<div class="form-group">
									    <label for="selectNam">Chọn đội</label>
									    <select class="form-control">
										  	<option value="A">A</option>
										  	<option value="B">B</option>
										  	<option value="C">C</option>
										  	<option value="D">D</option>
										  	<option value="E">E</option>
										  	<option value="F">F</option>
										</select>
									</div>
									<div class="form-group">
										<label for="total">Tổng số người dự đoán</label>
									    <input type="number" class="form-control" id="" placeholder="">
									</div>
									<div class="form-group button-submit btn-submit">
									    <button type="submit" class="btn btn-default">Xác nhận</button>
									    <div class="modal-mqc">
										    <div class="modal-content">
										      	<div class="modal-header">
										        	<h4 class="modal-title" id="">Mã xác nhận</h4>
										      	</div>
										      	<div class="modal-body">
										      		<h5>Mã xác nhận <span>sadsa</span></h5>
										       		<p>Soạn tin nhắn theo cú pháp <span class="mess">absc X Y</span> gửi <span class="send-number">1244</span></p>
										      	</div>
										      	<div class="modal-footer">
										        	<button type="button" class="btn btn-close btn-default">Đóng</button>
										      	</div>
										    </div>
										</div>
									</div>
								<!-- </form> -->
							</div>
						</div>
					</div>
				
				</div>
			</div>
			<!-- end col-lg-7 -->

			<div class="col-lg-3">
				<?php require 'sidebar.php';?>
			</div>
		</div>
	</section>
</div>

