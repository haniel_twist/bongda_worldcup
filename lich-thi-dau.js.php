<script type="text/javascript">
    $(document).ready(function() {

        $('#schedule .list-item-schedule').owlCarousel({
            loop: false,
            margin: 8,
            autoplay: false,
            autoplayTimeout: 8000,
            slideBy: 1,
            items: 3,
            dots: false,
            responsive:{
                0:{
                    items:3,
                },
                768:{
                    items:4,
                },
                992:{
                    items:6,
                },
                1199:{
                    items: 9,
                }
            }
        });
        // -- Control -- 
        var schedule = $("#schedule .list-item-schedule").owlCarousel();
        // Next
        $('#schedule .next').click(function(){
            schedule.trigger('next.owl.carousel');
            return false;
        });
        // Prev
        $('#schedule .prev').click(function(){
            schedule.trigger('prev.owl.carousel');
            return false;
        });

    });
    
</script>