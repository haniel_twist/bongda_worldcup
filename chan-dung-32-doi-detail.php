<!-- page chan dung 32 doi -->
<div class="page wrap-main-content">
	<section class="container">
		<div class="advertise-top">
			<a href="#" title=""><img src="assets/img/home/advertise-tiki.jpg" alt="" title=""></a>
		</div>
		<nav class="breadcrumb list-breadcrumb">
		  	<a class="breadcrumb-item" href="#"><i class="fa fa-home"></i></a>
		  	<span class="breadcrumb-item active">Chân dung 32 đội</span>
		</nav>
		<!-- breadcrumb -->

		<div class="row row-0">
			<div class="col-lg-2">
				<div class="advertise-left">
					<div class="advertise">
						<a href="#">
							<img src="assets/img/alowwatch.jpg" alt="advertise" title="advertise">
						</a>
					</div>
					<div class="advertise">
						<a href="#">
							<img src="assets/img/adam.jpg" alt="advertise" title="advertise">
						</a>
					</div>
				</div>		
			</div> 
			<!-- end col-lg-2 -->

			<div class="col-lg-7">
				<div class="main-content main-content-page page-team-fooball-detail">
					<div class="details-team">
						<div class="item-center">
							<div class="item-left">
								<div class="image">
									<div class="super-img loaded ratio-3x2" style="background-image: url('assets/img/home/team/netherlands.jpg')">
	                                    <img src="assets/img/home/team/netherlands.jpg" alt="">
	                                </div>
								</div>
							</div>
							<div class="title title-name">
								Tên đội bóng
								<span>Đội tuyển Bóng đá Quốc gia Brasil</span>
							</div>
							<div class="title">
								HLV: <span>Tite</span>
							</div>
							<div class="title">
								Key player: <span>Tiền đạo</span>
							</div>
						</div>
						<div class="item-right">
							<div class="title">
								Đội hình:
							</div>
							<img src="assets/img/sidebar/img-swanpark.jpg" alt="">
						</div>
					</div>

					<div class="tab-info">
						<ul id="list-tab-info"> 
						   <li class="active"><a href="#info">Thông tin</a></li>
						   <li><a href="#b">Lịch sử thi đấu</a></li>
						   <li><a href="#c">Lịch sử</a></li>
						</ul> 
						<!-- content a-->
						<div class="content" id="info">
							<div class="list-info">
								<div class="item-info">
									<div class="title">
										<div class="title-left">Tuần 24</div>
										<div class="title-right"><span>23:00</span> <span>13/06/2018</span></div>
									</div>
									<div class="content-info">
										<div class="item item-left">
											<div class="image">
												<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/team/netherlands.jpg')">
				                                    <img src="assets/img/home/team/netherlands.jpg" alt="">
				                                </div>
											</div>
											<p>NGA</p>
										</div>
										<div class="item-center">
											<div class="score">
												<span class="left">1</span>
												<span class="center">-</span>
												<span class="right">0</span>
											</div>
											<div class="ft">ft</div>
											<div class="detail">
												<a href="#">Chi tiết</a>
											</div>
										</div>
										<div class="item item-right">
											<div class="image">
												<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/team/netherlands.jpg')">
				                                    <img src="assets/img/home/team/netherlands.jpg" alt="">
				                                </div>
											</div>
											<p>NGA</p>
										</div>
									</div>
								</div>

								<div class="item-info">
									<div class="title">
										<div class="title-left">Tuần 24</div>
										<div class="title-right"><span>23:00</span> <span>13/06/2018</span></div>
									</div>
									<div class="content-info">
										<div class="item item-left">
											<div class="image">
												<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/team/netherlands.jpg')">
				                                    <img src="assets/img/home/team/netherlands.jpg" alt="">
				                                </div>
											</div>
											<p>NGA</p>
										</div>
										<div class="item-center">
											<div class="score">
												<span class="left">1</span>
												<span class="center">-</span>
												<span class="right">0</span>
											</div>
											<div class="ft">ft</div>
											<div class="detail">
												<a href="#">Chi tiết</a>
											</div>
										</div>
										<div class="item item-right">
											<div class="image">
												<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/team/netherlands.jpg')">
				                                    <img src="assets/img/home/team/netherlands.jpg" alt="">
				                                </div>
											</div>
											<p>NGA</p>
										</div>
									</div>
								</div>

								<div class="item-info">
									<div class="title">
										<div class="title-left">Tuần 24</div>
										<div class="title-right"><span>23:00</span> <span>13/06/2018</span></div>
									</div>
									<div class="content-info">
										<div class="item item-left">
											<div class="image">
												<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/team/netherlands.jpg')">
				                                    <img src="assets/img/home/team/netherlands.jpg" alt="">
				                                </div>
											</div>
											<p>NGA</p>
										</div>
										<div class="item-center">
											<div class="score">
												<span class="left">1</span>
												<span class="center">-</span>
												<span class="right">0</span>
											</div>
											<div class="ft">ft</div>
											<div class="detail">
												<a href="#">Chi tiết</a>
											</div>
										</div>
										<div class="item item-right">
											<div class="image">
												<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/team/netherlands.jpg')">
				                                    <img src="assets/img/home/team/netherlands.jpg" alt="">
				                                </div>
											</div>
											<p>NGA</p>
										</div>
									</div>
								</div>

								<div class="item-info">
									<div class="title">
										<div class="title-left">Tuần 24</div>
										<div class="title-right"><span>23:00</span> <span>13/06/2018</span></div>
									</div>
									<div class="content-info">
										<div class="item item-left">
											<div class="image">
												<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/team/netherlands.jpg')">
				                                    <img src="assets/img/home/team/netherlands.jpg" alt="">
				                                </div>
											</div>
											<p>NGA</p>
										</div>
										<div class="item-center">
											<div class="score">
												<span class="left">1</span>
												<span class="center">-</span>
												<span class="right">0</span>
											</div>
											<div class="ft">ft</div>
											<div class="detail">
												<a href="#">Chi tiết</a>
											</div>
										</div>
										<div class="item item-right">
											<div class="image">
												<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/team/netherlands.jpg')">
				                                    <img src="assets/img/home/team/netherlands.jpg" alt="">
				                                </div>
											</div>
											<p>NGA</p>
										</div>
									</div>
								</div>

								<div class="item-info">
									<div class="title">
										<div class="title-left">Tuần 24</div>
										<div class="title-right"><span>23:00</span> <span>13/06/2018</span></div>
									</div>
									<div class="content-info">
										<div class="item item-left">
											<div class="image">
												<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/team/netherlands.jpg')">
				                                    <img src="assets/img/home/team/netherlands.jpg" alt="">
				                                </div>
											</div>
											<p>NGA</p>
										</div>
										<div class="item-center">
											<div class="score">
												<span class="left">1</span>
												<span class="center">-</span>
												<span class="right">0</span>
											</div>
											<div class="ft">ft</div>
											<div class="detail">
												<a href="#">Chi tiết</a>
											</div>
										</div>
										<div class="item item-right">
											<div class="image">
												<div class="super-img loaded ratio-16x9" style="background-image: url('assets/img/home/team/netherlands.jpg')">
				                                    <img src="assets/img/home/team/netherlands.jpg" alt="">
				                                </div>
											</div>
											<p>NGA</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
			<!-- end col-lg-7 -->

			<div class="col-lg-3">
				<?php require 'sidebar.php';?>
			</div>
		</div>
	</section>
</div>

